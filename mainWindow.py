from kivy.uix.boxlayout import BoxLayout

from libraryPkg.library import Library
from libraryPkg.command import Commands
from windowsPkg.accountWindow import AccountController, AccountBudgetController, AccountManagementController
from windowsPkg.helpWindow import HelpController
from windowsPkg.loginWindow import LoginController
from windowsPkg.myBooksWindow import MyBooksController
from windowsPkg.salesWindow import SalesController, HistoricSBController
from windowsPkg.sendBooksWindow import SendBooksController, HistoricCommandController


class MainWindow(BoxLayout):
    def __init__(self, app, **kwargs):
        super().__init__(**kwargs)
        self._app = app
        self._old_view = None
        self._current_view = None
        self.current_view_name = ""
        self.background_color = (1, 1, 1, 1)
        self.library = Library()
        self.command = Commands()

        self.login_controller = LoginController(self)
        self.account_controller = AccountController(self)
        self.budget_controller = AccountBudgetController(self)
        self.management_controller = AccountManagementController(self)
        self.myBooks_controller = MyBooksController(self)
        self.sendBooks_controller = SendBooksController(self)
        self.historicCommands_controller = HistoricCommandController(self)
        self.sales_controller = SalesController(self)
        self.historicSB_controller = HistoricSBController(self)
        self.help_controller = HelpController(self)
        self.change_view("login", clear_widgets=False)

    def change_view(self, value: str, clear_widgets=True):
        self.current_view_name = value
        if clear_widgets:
            self.clear_widgets()
        self._old_view = self._current_view
        if value == "login":
            self._current_view = self.login_controller.view
        elif value == "account":
            self._current_view = self.account_controller.view
        elif value == "budget":
            self._current_view = self.budget_controller.view
        elif value == "management":
            self._current_view = self.management_controller.view
        elif value == "myBooks":
            self._current_view = self.myBooks_controller.view
        elif value == "sendBooks":
            self._current_view = self.sendBooks_controller.view
        elif value == "sales":
            self._current_view = self.sales_controller.view
        elif value == "borrows":
            self._current_view = self.sales_controller.view2
        elif value == "historicSales":
            self._current_view = self.historicSB_controller.view
        elif value == "historicBorrows":
            self._current_view = self.historicSB_controller.view2
        elif value == "help":
            self._current_view = self.help_controller.view
        elif value == "historicCommands":
            self._current_view = self.historicCommands_controller.view
        self.add_widget(self._current_view)
