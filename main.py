from kivy.app import App

from mainWindow import MainWindow


class LibraryApp(App):
    def build(self):
        self._mainWindow = MainWindow(self)
        self.title = "Librairie"
        return self._mainWindow

# TODO: Test!
if __name__ == "__main__":
    LibraryApp().run()
