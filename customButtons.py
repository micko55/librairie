from kivy.uix.image import Image
from kivy.uix.button import Button


class ButtonBehavior(object):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.forced_state = ""

    def _do_release(self, *args):
        if self.forced_state != "":
            self.state = self.forced_state
            self.background_color = (.4, .733, 1, 1)
            print("bouh.")
        else:
            self.state = "normal"


class SupButton(Button):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.original_font_size = self.font_size
        self.forced_down = False

    def set_forced_down(self, value: bool):
        self.forced_down = value
        if value:
            self.background_color = (.4, .733, 1, 1)
        elif not self.get_disabled():
            self.background_color = (1, 1, 1, 1)


class CustomButton(SupButton):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.background_normal = 'atlas://images/defaulttheme/button'
        self.background_down = 'atlas://images/defaulttheme/button'
        self.disabled_color = (0, 0, 0, 1)

    def on_press(self):
        self.background_color = (.4, .733, 1, 1)

    def on_touch_up(self, touch):
        super().on_touch_up(touch)
        if not self.forced_down:
            self.background_color = (1, 1, 1, 1)


class CustomBlueButton(SupButton):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.background_normal = "images/blueButton.png"
        self.background_down = "images/blueButtonPressed.png"
        self.bold = True


class NudeButton(SupButton):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.background_normal = ""
        self.background_down = ""
        self.background_disabled_down = ""
        self.background_disabled_normal = ""
        self.background_color = (0, 0, 0, 0)
        self.color = (0, 0, 0, 1)


class SearchButton(CustomButton):
    def __init__(self, **kwargs):
        super(SearchButton, self).__init__(**kwargs)
        self.image = Image(source="images/search.png", pos_hint=self.pos_hint, size=self.size, allow_stretch=True,
                           keep_ratio=False)
        self.add_widget(self.image, index=0)
