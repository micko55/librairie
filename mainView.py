from kivy.core.window import Window
from kivy.graphics import Rectangle, Color, Line
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

from customButtons import CustomButton, CustomBlueButton, NudeButton, SearchButton
from windowsPkg.popupWindow import CustomPopup, PopupContent


class MainView(FloatLayout):
    def __init__(self, controller, view, **kwargs):
        super().__init__(**kwargs)
        self.controller = controller
        self.view = view
        Window.minimum_width = 600
        Window.minimum_height = 600
        Window.size = (600, 600)
        Window.bind(on_show=self.resize_widgets,
                    on_resize=self.resize_widgets,
                    on_maximize=self.resize_widgets,
                    on_restore=self.resize_widgets)
        self.size = Window.size
        self.view.size = self.size
        with self.canvas.before:
            Color(1, 1, 1, 1)
            Rectangle(size=self.size, pos=self.pos)
        self._menu_buttons = []
        self.create_menu_buttons()

    @property
    def menu_buttons(self):
        return self._menu_buttons

    @menu_buttons.setter
    def menu_buttons(self, value):
        self._menu_buttons = value

    def create_menu_buttons(self):
        self.menu_buttons.clear()
        button = self.create_button(self, "Mon Compte", x=0, y=.9, width=.2, height=.1, array=False)
        button.id = "account"
        self._menu_buttons.append(button)
        button = self.create_button(self, "Mes Livres", x=.2, y=.9, width=.2, height=.1, array=False)
        button.id = "myBooks"
        self._menu_buttons.append(button)
        button = self.create_button(self, "Envoyer", x=.4, y=.9, width=.2, height=.1, array=False)
        button.id = "sendBooks"
        self._menu_buttons.append(button)
        button = self.create_button(self, "Ventes & Emprunts", x=.6, y=.9, width=.3, height=.1, array=False)
        button.id = "sales"
        self._menu_buttons.append(button)
        button = self.create_button(self, "?", x=.9, y=.9, width=.1, height=.1, array=False)
        button.id = "help"
        self._menu_buttons.append(button)
        [b.bind(on_release=self.controller.change_view) for b in self._menu_buttons]

    def hide_menu(self):
        for b in self._menu_buttons:
            b.opacity = 0
            b.set_disabled(True)

    def show_menu(self):
        for b in self._menu_buttons:
            b.opacity = 1
            b.set_disabled(False)

    @staticmethod
    def create_button(view, name: str, x: float, y: float, width: float, height: float, font_size=14, array=True,
                      rotation=0, color=(1, 1, 1, 1)):
        button = CustomButton(text=name, pos_hint={"x": x, "y": y}, size_hint=(width, height),
                              font_size=font_size * (((Window.width + Window.height) / 2) / 600), color=color)
        view.add_widget(button, index=len(view.children))
        if array:
            view.buttons.append(button)
            pass
        return button

    @staticmethod
    def create_search_button(view, x: float, y: float, width: float, height: float, array=True):
        button = SearchButton(pos_hint={"x": x, "y": y}, size_hint=(width, height))
        view.add_widget(button, index=len(view.children))
        if array:
            view.buttons.append(button)
        return button

    @staticmethod
    def create_nude_button(view, name: str, x: float, y: float, width: float, height: float, font_size=10, array=True,
                           rotation=0):
        button = NudeButton(text=name, pos_hint={"x": x, "y": y}, size_hint=(width, height),
                            font_size=font_size * (((Window.width + Window.height) / 2) / 600))
        view.add_widget(button, index=len(view.children))
        if array:
            view.buttons.append(button)
        return button

    @staticmethod
    def create_blue_button(view, name: str, x: float, y: float, width: float, height: float, font_size=14, array=True,
                           rotation=0):
        button = CustomBlueButton(text=name, pos_hint={"x": x, "y": y}, size_hint=(width, height),
                                  font_size=font_size * (((Window.width + Window.height) / 2) / 600))
        button.original_font_size = font_size
        view.add_widget(button, index=len(view.children))
        if array:
            view.buttons.append(button)
        return button

    @staticmethod
    def create_textInput(view, x, y, width, height, password=False, intOnly=False, multiline=False, array=True,
                         font_size=10):
        if intOnly:
            textInput = TextInput(pos_hint={"x": x, "y": y}, size_hint=(width, height), input_filter='int',
                                  password=password, multiline=multiline,
                                  font_size=font_size * (((Window.width + Window.height) / 2) / 600))
        else:
            textInput = TextInput(pos_hint={"x": x, "y": y}, size_hint=(width, height), password=password,
                                  multiline=multiline,
                                  font_size=font_size * (((Window.width + Window.height) / 2) / 600))
        textInput.original_font_size = font_size
        view.add_widget(textInput, index=len(view.children))
        if array:
            view.textInputs.append(textInput)
        return textInput

    @staticmethod
    def create_label(view, name, x, y, width, height, halign="left", valign="center", padding_x=0, font_size=12,
                     color=(0, 0, 0, 1), array=True, bold=False, bg_color=(1, 1, 1, 0)):
        h = height * (view.size_hint[1] * Window.height)
        w = width * (view.size_hint[0] * Window.width)
        label = Label(text=name, pos_hint={"x": x, "y": y}, size_hint=(width, height), color=color,
                      font_size=font_size * (((Window.width + Window.height) / 2) / 600),
                      text_size=(w, h), valign=valign, halign=halign, padding_x=padding_x, bold=bold)
        label.original_font_size = font_size
        """rect_color = Color(bg_color[0], bg_color[1], bg_color[2], bg_color[3])
        label.canvas.before.clear()
        label.canvas.before.add(rect_color)
        label.canvas.before.add(Rectangle(size=(w, h), pos=(view.x + x * view.width, view.y + y * view.height)))"""
        view.add_widget(label, index=len(view.children))
        if array:
            view.labels.append(label)
        return label

    @staticmethod
    def create_rounded_line(view, x, y, width, height, color=(0, 0, 0, 1)):
        view.canvas.before.add(Color(color[0], color[1], color[2], color[3]))
        view.canvas.before.add(
            Line(rounded_rectangle=(x * view.width, y * view.height, width * view.width, height * view.height, 20, 50)))

    @staticmethod
    def create_line(view, x, y, width, height, color=(0, 0, 0, 1)):
        view.canvas.before.add(Color(color[0], color[1], color[2], color[3]))
        view.canvas.before.add(
            Line(rectangle=(x * view.width, y * view.height, width * view.width, height * view.height)))

    @staticmethod
    def create_full_rectangle(view, x, y, width, height, color=(0, 0, 0, 1)):
        view.canvas.before.add(Color(color[0], color[1], color[2], color[3]))
        view.canvas.before.add(Rectangle(pos=(x * view.width, y * view.height),
                                         size=(width * view.width, height * view.height)))

    def create_labels(self):
        pass

    def create_buttons(self):
        pass

    def create_textInputs(self):
        pass

    def create_popups(self):
        pass

    @staticmethod
    def create_popup(view, title="", pos_hint=None, size_hint=(1, 1), rounded=False, array=True, title_size=20):
        popup = CustomPopup(view, title=title, pos_hint=pos_hint, size_hint=size_hint, rounded=rounded,
                            title_size=title_size)
        popup.content = PopupContent(popup, pos_hint={"x": 0, "y": 0}, size_hint=(1, 1))
        popup.content.view = view
        if array:
            view.popups.append(popup)
        return popup

    @staticmethod
    def resize_widget(widget):
        if widget.__class__ == Label:
            w = widget.size_hint[0] * (widget.parent.size_hint[0] * Window.width)
            h = widget.size_hint[1] * (widget.parent.size_hint[1] * Window.height)
            widget.text_size = (w, h)
        if hasattr(widget, "original_font_size"):
            widget.font_size = widget.original_font_size * (((Window.width + Window.height) / 2) / 600)

    def resize_widgets(self, window=None, width=None, height=None):
        self.size = Window.size
        self.view.size = self.size
        self.canvas.before.clear()
        with self.canvas.before:
            Color(1, 1, 1, 1)
            Rectangle(size=self.size, pos=self.pos)
        for widget in self.children:
            self.resize_widget(widget)
        if self.view.popups:
            for popup in self.view.popups:
                popup.title_size = 14 * (((Window.width + Window.height) / 2) / 600)
                for children in popup.content.children:
                    self.resize_widget(children)
