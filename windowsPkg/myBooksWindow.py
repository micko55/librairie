from math import floor, ceil

from kivy.core.window import Window
from mainController import MainController
from mainView import MainView


class MyBooksController(MainController):
    def __init__(self, mainWindow):
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = MyBooksView(self)
        super().__init__(mainWindow, self.view)

    def show_book_info(self, book, touch):
        if book.on_touch_down(touch):
            touch.ungrab(book)
            popup = self.view.popups[0]
            popup.title = book.title
            popup.content.labels[0].text = "Auteur : " + book.author
            popup.content.labels[1].text = "Genre : " + book.genre
            popup.content.labels[2].text = "Prix : " + str(book.price)
            popup.content.labels[3].text = "Code-Barres : " + str(book.code_barres)
            popup.content.labels[4].text = "Emplacement : " + str(book.storage.name) + "-" + str(book.shelf.index + 1) \
                                           + " : " + str(book.number + 1)
            popup.book = book
            self.open_popup(self.view, 0)

    def sell_book(self, button):
        new_controller = self.mainWindow.sales_controller
        popup = self.view.popups[0]
        book = popup.book
        if book is not None:
            new_controller.view.textInputs[1].text = book.title
            new_controller.view.textInputs[2].text = book.author
            new_controller.view.textInputs[3].text = book.price
            new_controller.view.textInputs[4].text = book.code_barres
            new_controller.view.textInputs[5].text = str(book.storage.name)
            new_controller.view.textInputs[6].text = str(book.shelf.index + 1)
            new_controller.view.textInputs[7].text = str(book.number + 1)
            if book.borrowed == False and book.sold == False:
                new_controller.view.labels[14].text = " OUI "
            elif book.sold == True:
                new_controller.view.labels[14].text = " NON "
            elif book.borrowed == True:
                new_controller.view.labels[14].text = " INDISPONIBLE. RETOUR LE XX/XX/XXXX "

            self.close_popup(self.view.popups[0].content.buttons[0])
            popup.book = None
            self.mainWindow.change_view("sales")

    def borrow_book(self, button):
        new_controller = self.mainWindow.sales_controller
        popup = self.view.popups[0]
        book = popup.book
        if book is not None:
            new_controller.view2.textInputs[1].text = book.title
            new_controller.view2.textInputs[2].text = book.author
            new_controller.view2.textInputs[3].text = book.price
            new_controller.view2.textInputs[4].text = book.code_barres
            new_controller.view2.textInputs[5].text = book.storage.name
            new_controller.view2.textInputs[6].text = str(book.shelf.index + 1)
            new_controller.view2.textInputs[7].text = str(book.number + 1)
            if book.borrowed == False and book.sold == False:
                new_controller.view2.labels[14].text = " OUI "
            elif book.sold == True:
                new_controller.view2.labels[14].text = " NON "
            elif book.borrowed == True:
                new_controller.view2.labels[14].text = " INDISPONIBLE. RETOUR LE XX/XX/XXXX "
            self.close_popup(self.view.popups[0].content.buttons[0])
            popup.book = None
            self.mainWindow.change_view("borrows")

    def scroll_up(self, button):
        library = self.view.library
        current_storage = self.view.current_storage
        current_shelf = self.view.current_shelf
        changed = False
        if self.view.search_text != "" or self.view.sorted_by != "" or self.view.filter:
            books = self.view.current_books
            length = ceil(len(books) / 20)
            y = round((.7 - (self.view.scroll_y - .05)) / 70 * (length - 3) * 100)
            # print(length, y, self.view.scroll_y)
            if length > 3 and y > 0:
                self.view.scroll_y = .05 + .7 - (.7 * (y - 1) / (length - 3))
                # print(self.view.scroll_y)
                self.view.scroll_button.pos_hint = {"x": self.view.scroll_button.pos_hint["x"], "y": self.view.scroll_y}
                self.view.update(True)
            return
        else:
            if current_storage.index > 0 or current_shelf.index > 0:
                if current_shelf.index > 2:
                    self.view.current_shelf = current_storage.shelves[current_shelf.index - 3]
                    changed = True
                elif current_storage.index > 0 and current_shelf.index < 3:
                    self.view.current_storage = library.storages[current_storage.index - 1]
                    self.view.current_shelf = self.view.current_storage.shelves[9 + current_shelf.index - 2]
                    changed = True
                else:
                    base_i = self.view.current_shelf.index
                    i = base_i
                    while self.view.current_shelf.index - 1 >= 0:
                        self.view.current_shelf = self.view.current_storage.shelves[self.view.current_shelf.index - 1]
                        i -= 1
                        if i - 1 < base_i - 3 or i - 1 < 0:
                            break
                    changed = True
        if changed:
            self.view.update()

    def scroll_down(self, button):
        library = self.view.library
        current_storage = self.view.current_storage
        current_shelf = self.view.current_shelf
        changed = False
        if self.view.search_text != "" or self.view.sorted_by != "" or self.view.filter:
            books = self.view.current_books
            length = ceil(len(books) / 20)
            y = round((.7 - (self.view.scroll_y - .05)) / 70 * (length - 3) * 100)
            # print(length, y, self.view.scroll_y)
            if length > 3 and y < length - 3:
                self.view.scroll_y = .05 + .7 - (.7 * (y + 1) / (length - 3))
                # print(self.view.scroll_y)
                self.view.scroll_button.pos_hint = {"x": self.view.scroll_button.pos_hint["x"], "y": self.view.scroll_y}
                self.view.update(True)
            return
        else:
            if current_storage.index < 9 or (current_storage.index == 9 and current_shelf.index < 7):
                if (current_shelf.index < 7 and current_storage.index < 9) or (current_shelf.index < 5
                                                                               and current_storage.index == 9):
                    self.view.current_shelf = current_storage.shelves[current_shelf.index + 3]
                    changed = True
                elif current_storage.index < 9 and current_shelf.index > 6:
                    self.view.current_storage = library.storages[current_storage.index + 1]
                    self.view.current_shelf = self.view.current_storage.shelves[current_shelf.index - 9 + 2]
                    changed = True
                elif current_storage.index == 9 and current_shelf.index < 7:
                    i = self.view.current_shelf.index
                    while i < 7:
                        self.view.current_shelf = self.view.current_storage.shelves[self.view.current_shelf.index + 1]
                        i += 1
                        changed = True
        if changed:
            self.view.update()

    def move_scroll_button(self, button, touch):
        touch_pos = touch.pos
        if not button.collide_point(*(touch_pos[0], button.pos[1])) and \
                not button.collide_point(*(touch_pos[0], button.pos[1])):
            return
        current_books = self.view.current_books
        if len(current_books) < 60:
            return
        y = max(min(touch.spos[1], .75), .05)
        self.view.scroll_y = y
        button.pos_hint = {"x": button.pos_hint["x"], "y": y}
        if self.view.search_text != "" or self.view.sorted_by != "" or self.view.filter:
            self.view.update(True)
            return
        y = floor((.7 - (y - .05)) / 70 * 97 * 100)
        old_storage = self.view.current_storage
        old_shelf = self.view.current_shelf
        self.view.current_storage = self.view.library.storages[floor(y / 10)]
        self.view.current_shelf = self.view.current_storage.shelves[floor(y) % 10]
        if old_storage != self.view.current_storage or old_shelf != self.view.current_shelf:
            self.view.update()

    def move_scroll_button_to_top(self):
        view = self.view
        view.current_storage = view.library.storages[0]
        view.current_shelf = view.current_storage.shelves[0]
        view.scroll_y = .75
        scroll_button = self.view.scroll_button
        scroll_button.pos_hint = {"x": scroll_button.pos_hint["x"], "y": .75}
        self.view.update(based_on_scroll=True)

    def update_search(self, text_input, text):
        parent = text_input.parent
        parent.search_text = text
        books = parent.library.books
        if text != "":
            for i in range(3):
                if parent.buttons[i] is not None:
                    parent.remove_widget(parent.buttons[i])
                    parent.buttons[i] = None
            books = parent.library.search(text)
        parent.current_books = books
        parent.set_shown_books(books)
        self.move_scroll_button_to_top()

    def show_sort_by(self, button):
        self.open_popup(self.view, 1)

    def reset_search(self, button):
        self.view.textInputs[0].text = ""
        self.update_search(self.view.textInputs[0], "")
        self.view.update()

    def sort_book_by_title(self, button):
        library = self.view.library
        books = self.view.current_books
        library.sort_by(books, "title", button.text != "A->Z")
        self.view.sorted_by = "title"
        self.view.current_books = books
        self.move_scroll_button_to_top()
        self.view.update()

    def sort_book_by_author(self, button):
        library = self.view.library
        books = self.view.current_books
        library.sort_by(books, "author", button.text != "A->Z")
        self.view.sorted_by = "author"
        self.view.current_books = books
        self.move_scroll_button_to_top()
        self.view.update()

    def sort_book_by_price(self, button):
        library = self.view.library
        books = self.view.current_books
        library.sort_by(books, "price", button.text != "1->999")
        self.view.sorted_by = "price"
        self.view.current_books = books
        self.move_scroll_button_to_top()
        self.view.update()

    def sort_book_reset(self, button):
        library = self.view.library
        books = self.view.current_books
        library.sort_by(books, "title")
        library.sort_by(books, "author")
        self.view.sorted_by = ""
        self.view.current_books = books
        self.move_scroll_button_to_top()
        self.view.update()

    def show_filtered_by(self, button):
        self.open_popup(self.view, 2)

    def filter_by_title(self, textInput):
        pass

    def filter_by_author(self, textInput):
        pass

    @staticmethod
    def filter_by_storage(button):
        button.set_forced_down(not button.forced_down)

    @staticmethod
    def filter_by_shelf(button):
        button.set_forced_down(not button.forced_down)

    def filter_by(self, button):
        library = self.view.library
        popupcontent = button.parent
        filtered = False
        if not self.view.filter:
            self.view.books_before_filter = self.view.current_books.copy()
        searchedBooks = self.view.books_before_filter.copy()
        storageBooks = []
        for button in popupcontent.buttons[:10]:
            if button.forced_down:
                storageBooks += library.search_in(searchedBooks, "storage", button.id).copy()
                filtered = True
        if storageBooks:
            searchedBooks = storageBooks
        shelfBooks = []
        for button in popupcontent.buttons[10:20]:
            if button.forced_down:
                shelfBooks += library.search_in(searchedBooks, "shelf", str(button.id)).copy()
                filtered = True
        if shelfBooks:
            searchedBooks = shelfBooks
        titleBooks = []
        if popupcontent.textInputs[0].text != "":
            titleBooks = library.search_in(searchedBooks, "title", popupcontent.textInputs[0].text).copy()
            filtered = True
        if titleBooks:
            searchedBooks = titleBooks
        authorBooks = []
        if popupcontent.textInputs[1].text != "":
            authorBooks = library.search_in(searchedBooks, "author", popupcontent.textInputs[1].text).copy()
            filtered = True
        if authorBooks or popupcontent.textInputs[1].text != "":
            # searchedBooks = authorBooks
            books = authorBooks
        elif titleBooks or popupcontent.textInputs[0].text != "":
            books = titleBooks
        elif shelfBooks:
            books = shelfBooks
        elif storageBooks:
            books = storageBooks
        else:
            books = searchedBooks
        if not filtered:
            self.view.filter = False
            books = self.view.books_before_filter.copy()
        else:
            self.view.filter = True
        self.view.current_books = books.copy()
        print(len(books))
        self.close_popup(button)
        self.move_scroll_button_to_top()

    def reset_filter(self, button):
        popupcontent = button.parent
        popupcontent.textInputs[0].text = ""
        popupcontent.textInputs[1].text = ""
        for button in popupcontent.buttons:
            button.set_forced_down(False)
        if self.view.filter:
            self.view.current_books = self.view.books_before_filter.copy()
        self.view.filter = False
        self.move_scroll_button_to_top()


class MyBooksView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.filter = False
        self.sorted_by = ""
        self.search_text = ""
        self.library = self.controller.mainWindow.library
        self.current_books = self.library.books.copy()
        self.books_before_filter = self.current_books.copy()
        self.current_storage = self.library.storages[0]
        self.current_shelf = self.current_storage.shelves[0]
        self.shown_books = self.current_storage.shelves[0].books + self.current_storage.shelves[1].books + \
                           self.current_storage.shelves[2].books
        self.create_labels()
        self.create_textInputs()
        self.create_buttons()
        self.create_lines()
        self.scroll_button = None
        self.scroll_y = 0
        self.create_scroll()
        self.create_popups()
        self.show_books()

    def create_textInputs(self):
        textInput = self.create_textInput(self, 0, .85, .75, .05, font_size=12)
        textInput.bind(text=self.controller.update_search)

    def create_buttons(self):
        self.create_library_buttons()
        button = self.create_button(self, "X", .75, .85, .05, .05, font_size=16)
        button.bind(on_release=self.controller.reset_search)
        button = self.create_button(self, "Trier", .8, .85, .1, .05, font_size=14)
        button.bind(on_release=self.controller.show_sort_by)
        button = self.create_button(self, "Filtrer", .9, .85, .1, .05, font_size=14)
        button.bind(on_release=self.controller.show_filtered_by)

    def create_library_buttons(self, a1="A-1", a2="A-2", a3="A-3"):
        if len(self.buttons) < 3:
            for i in range(3):
                self.buttons.append(None)
        self.buttons[0] = self.create_button(self, a1, 0, .58, .1, .1, font_size=16, color=(0, 0, 0, 1), array=False)
        self.buttons[1] = self.create_button(self, a2, 0, .3, .1, .1, font_size=16, color=(0, 0, 0, 1), array=False)
        self.buttons[2] = self.create_button(self, a3, 0, .02, .1, .1, font_size=16, color=(0, 0, 0, 1), array=False)
        [self.buttons[i].set_disabled(True) for i in range(3)]

    def create_lines(self):
        self.create_full_rectangle(self, 0, .57, .9, .008, (.4, .733, 1, 1))
        self.create_full_rectangle(self, 0, .29, .9, .008, (.4, .733, 1, 1))
        self.create_full_rectangle(self, 0, .01, .9, .008, (.4, .733, 1, 1))

    def create_scroll(self):
        self.create_line(self, .95, 0, .05, .85, (.4, .733, 1, 1))
        button = self.create_button(self, "^", .95, .8, .05, .05, font_size=16)
        button.bind(on_release=self.controller.scroll_up)
        button = self.create_button(self, "v", .95, 0, .05, .05, font_size=16)
        button.bind(on_release=self.controller.scroll_down)
        self.scroll_button = self.create_blue_button(self, "", .95, .75 - .7 * (self.scroll_y / 97), .05, .05)
        self.scroll_button.bind(on_touch_move=self.controller.move_scroll_button)
        pass

    def create_popups(self):
        # LIVRE
        popup = self.create_popup(self, "Nom du Livre", pos_hint={"center_x": .5, "center_y": .5}, size_hint=(.5, .5),
                                  rounded=True)
        for i in range(5):
            self.create_label(popup.content, "", 0, .9 - i / 10, 1, .1, halign="center", valign="middle")
        button = self.create_blue_button(popup.content, "Vendre", .05, .2, .45, .15)
        button.id = "sales"
        button.bind(on_release=self.controller.sell_book)
        button = self.create_blue_button(popup.content, "Emprunter", .55, .2, .45, .15)
        button.id = "borrows"
        button.bind(on_release=self.controller.borrow_book)
        button = self.create_blue_button(popup.content, "OK", .05, 0, .95, .15)
        button.bind(on_release=self.controller.close_popup)
        # TRIER
        popup = self.create_popup(self, "Trier", pos_hint={"x": .5, "y": .45}, size_hint=(.4, .4))
        label = self.create_label(popup.content, "Par Titre", 0, .9, 1, .1, halign="center")
        button = self.create_button(popup.content, "A->Z", 0, .8, .45, .1)
        button.bind(on_release=self.controller.sort_book_by_title)
        button = self.create_button(popup.content, "Z->A", .55, .8, .45, .1)
        button.bind(on_release=self.controller.sort_book_by_title)
        label = self.create_label(popup.content, "Par Auteur", 0, .7, 1, .1, halign="center")
        button = self.create_button(popup.content, "A->Z", 0, .6, .45, .1)
        button.bind(on_release=self.controller.sort_book_by_author)
        button = self.create_button(popup.content, "Z->A", .55, .6, .45, .1)
        button.bind(on_release=self.controller.sort_book_by_author)
        label = self.create_label(popup.content, "Par Prix", 0, .5, 1, .1, halign="center")
        button = self.create_button(popup.content, "1->999", 0, .4, .45, .1)
        button.bind(on_release=self.controller.sort_book_by_price)
        button = self.create_button(popup.content, "999->1", .55, .4, .45, .1)
        button.bind(on_release=self.controller.sort_book_by_price)
        button = self.create_blue_button(popup.content, "Réinitialiser", .25, .2, .5, .15)
        button.bind(on_release=self.controller.sort_book_reset)
        button = self.create_blue_button(popup.content, "OK", 0, 0, 1, .15)
        button.bind(on_release=self.controller.close_popup)
        # FILTRER
        popup = self.create_popup(self, "Filtrer", pos_hint={"x": .6, "y": .25}, size_hint=(.4, .6))
        self.create_label(popup.content, "Par Titre", 0, .9, 1, .1, halign="center")
        self.create_textInput(popup.content, 0, .82, 1, .08)
        self.create_label(popup.content, "Par Auteur", 0, .72, 1, .1, halign="center")
        self.create_textInput(popup.content, 0, .64, 1, .08)
        self.create_label(popup.content, "Par Armoire", 0, .54, 1, .1, halign="center")
        letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
        for i, letter in enumerate(letters):
            button = self.create_button(popup.content, letter, .1*i, .44, .1, .1)
            button.id = letter
            button.bind(on_release=self.controller.filter_by_storage)
        self.create_label(popup.content, "Par Étagère", 0, .34, 1, .1, halign="center")
        for i in range(10):
            button = self.create_button(popup.content, str(1+i), .1*i, .24, .1, .1)
            button.id = i
            button.bind(on_release=self.controller.filter_by_shelf)
        button = self.create_blue_button(popup.content, "Réinitialiser", .25, .12, .5, .1)
        button.bind(on_release=self.controller.reset_filter)
        button = self.create_blue_button(popup.content, "OK", 0, 0, 1, .1)
        button.bind(on_release=self.controller.filter_by)

    def show_books(self):
        windowWidth = Window.width
        windowHeight = Window.height
        for i, book in enumerate(self.shown_books):
            if book is not None:
                self.show_book(book, i, windowWidth, windowHeight)

    def show_book(self, book, i, windowWidth, windowHeight):
        y = .58
        pos = i
        if i > 19:
            pos -= floor(i / 20) * 20
            y -= .28 * floor(i / 20)
        book.size = (151 * (windowHeight / 600), 41 * (windowWidth / 600))
        book.label.text_size = (book.width * .8, book.height)
        book.label.font_size = book.label.original_font_size * ((windowWidth + windowHeight) / 2 / 600)
        book.pos = (.12 * windowWidth + ((22 * (windowWidth / 600)) * pos), y * windowHeight)
        book.bind(on_touch_down=self.controller.show_book_info)
        if book.parent != self:
            self.add_widget(book, index=1)

    def resize_widgets(self, window=None, width=None, height=None):
        super().resize_widgets(window, width, height)
        self.create_lines()
        if self.shown_books:
            windowWidth = Window.width
            windowHeight = Window.height
            for i, book in enumerate(self.shown_books):
                if book is not None:
                    y = .58
                    pos = i
                    if i > 19:
                        pos -= floor(i / 20) * 20
                        y -= .28 * floor(i / 20)
                    book.size = (151 * (windowHeight / 600), 41 * (windowWidth / 600))
                    book.label.text_size = (book.width * .8, book.height)
                    book.label.font_size = book.label.original_font_size * ((windowWidth + windowHeight) / 2 / 600)
                    book.pos = (.12 * windowWidth + ((22 * (windowWidth / 600)) * pos), y * windowHeight)

    def update(self, based_on_scroll=False):
        for i in range(3):
            if self.buttons[i] is not None:
                self.remove_widget(self.buttons[i])
                self.buttons[i] = None
        if self.search_text != "" or self.sorted_by != "" or self.filter:
            if based_on_scroll:
                if len(self.current_books) > 60:
                    books = self.current_books.copy()
                    length = ceil(len(books) / 20 - 3)
                    y = round((.7 - (self.scroll_y - .05)) / 70 * length * 100)
                    books = []
                    for i in range(60):
                        try:
                            books.append(self.current_books[y * 20 + i])
                        except IndexError:
                            break
                else:
                    books = self.current_books.copy()
                    self.scroll_y = .75
            else:
                books = self.current_books.copy()
                if len(self.current_books) > 60:
                    length = round(len(books) / 20 - 3)
                    y = round((.7 - (self.scroll_y - .05)) / 70 * length * 100)
                    books = []
                    for i in range(60):
                        try:
                            books.append(self.current_books[y * 20 + i])
                        except IndexError:
                            break
                    try:
                        self.scroll_button.pos_hint = {"x": self.scroll_button.pos_hint["x"],
                                                       "y": .75 - .7 * self.scroll_y / length}
                    except ZeroDivisionError:
                        self.scroll_button.pos_hint = {"x": self.scroll_button.pos_hint["x"],
                                                       "y": .75}
            self.set_shown_books(books)
            return
        self.scroll_y = self.current_storage.index * 10 + self.current_shelf.index
        self.scroll_button.pos_hint = {"x": self.scroll_button.pos_hint["x"], "y": .75 - .7 * self.scroll_y / 97}
        a1, a2, a3 = self.current_storage.name + "-" + str(self.current_shelf.index + 1), "A-2", "A-3"
        if self.current_shelf.index > 7:
            books = self.current_storage.shelves[self.current_shelf.index].books.copy()
            if self.current_shelf.index == 9 and self.current_storage.index != 9:
                a2 = self.library.storages[self.current_storage.index + 1].name + "-1"
                a3 = self.library.storages[self.current_storage.index + 1].name + "-2"
                books += self.library.storages[self.current_storage.index + 1].shelves[0].books
                books += self.library.storages[self.current_storage.index + 1].shelves[1].books
            elif self.current_shelf.index == 8:
                a2 = self.library.storages[self.current_storage.index].name + "-10"
                a3 = self.library.storages[self.current_storage.index + 1].name + "-1"
                books += self.current_storage.shelves[9].books
                if self.current_storage != 9:
                    books += self.library.storages[self.current_storage.index + 1].shelves[0].books
        else:
            books = self.current_storage.shelves[self.current_shelf.index].books + \
                    self.current_storage.shelves[self.current_shelf.index + 1].books + \
                    self.current_storage.shelves[self.current_shelf.index + 2].books
            a2 = self.current_storage.name + "-" + str(self.current_shelf.index + 2)
            a3 = self.current_storage.name + "-" + str(self.current_shelf.index + 3)
        self.create_library_buttons(a1, a2, a3)
        self.set_shown_books(books)

    def set_shown_books(self, books: list):
        for book in self.shown_books:
            if book is not None:
                self.remove_widget(book)
        self.shown_books = books.copy()
        self.show_books()
