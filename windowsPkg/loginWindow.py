import json

from mainController import MainController
from mainView import MainView


class LoginController(MainController):
    def __init__(self, mainWindow):
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = LoginView(self)
        super().__init__(mainWindow, self.view)

    def login(self, instance):
        users = self.get_users()
        if users:
            for user in users:
                if self.view.textInputs[0].text == user[0] and self.view.textInputs[1].text == user[1]:
                    self.view.textInputs[0].text = ""
                    self.view.textInputs[1].text = ""
                    self.mainWindow.change_view("account")
                    return
        if self.popup_shown is None:
            self.open_popup(self.view, 0)

    def forgot_password(self, instance):
        self.open_popup(self.view, 2)

    def reset_password(self, instance):
        reset_id = instance.parent.textInputs[0].text
        users = self.get_users()
        userFound = False
        if users:
            with open("users.json", 'w', encoding="utf-8") as f:
                json_dict = {"users": []}
                for user in users:
                    if user[0] == reset_id:
                        json_dict["users"].append([user[0], "azerty"])
                        userFound = True
                    else:
                        json_dict["users"].append([user[0], user[1]])
                json.dump(json_dict, f, indent=4, ensure_ascii=False)
        if userFound:
            self.close_popup(instance)
            self.open_popup(self.view, 1)
        else:
            if len(instance.parent.labels) < 2:
                self.view.create_label(instance.parent, "Identifiant incorrect",
                                       0, .6, 1, .4, halign="center", valign="middle", bold=True)

    def close_password_reset(self, instance):
        if len(instance.parent.labels) > 1:
            instance.parent.remove_widget(instance.parent.labels[1])
            instance.parent.labels.remove(instance.parent.labels[1])
        self.close_popup(instance)


class LoginView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.hide_menu()
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.create_rounded_line(self, .25, .175, .5, .4, (.4, .733, 1, 1))
        self.create_labels()
        self.create_textInputs()
        self.create_buttons()
        self.create_popups()

    def create_labels(self):
        self.create_label(self, name="Librairie", x=0, y=.5, width=1, height=.5, halign="center",
                          font_size=30)
        self.create_label(self, name="Connexion", x=.275, y=.45, width=.45, height=.1, halign="left", valign="top")
        self.create_label(self, name="Nom d'utilisateur :", x=.275, y=.4, width=.45, height=.1, halign="left",
                          valign="top", font_size=10)
        self.create_label(self, name="Mot de passe :", x=.275, y=.3, width=.45, height=.1, halign="left",
                          valign="top", font_size=10)

    def create_textInputs(self):
        self.create_textInput(self, x=.275, y=.41, width=.45, height=.05),
        self.create_textInput(self, x=.275, y=.31, width=.45, height=.05, password=True)

    def create_buttons(self):
        button = self.create_blue_button(self, name="SE CONNECTER", x=.275, y=.225, width=.45, height=.075)
        button.bind(on_release=self.controller.login)
        self.buttons.append(button)
        button = self.create_nude_button(self, name="Mot de passe oublié ?", x=.275, y=.2, width=.18, height=.03)
        button.bind(on_release=self.controller.forgot_password)
        self.buttons.append(button)

    def create_popups(self):
        popup = self.create_popup(self, pos_hint={"center_x": .5, "center_y": .4}, size_hint=(.55, .5),
                                  rounded=True)
        self.create_label(popup.content, "Identifiant ou mot de passe incorrect.", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button = self.create_blue_button(popup.content, "OK", 0, 0, 1, .2)
        button.bind(on_release=self.controller.close_popup)
        popup = self.create_popup(self, pos_hint={"center_x": .5, "center_y": .4}, size_hint=(.55, .5),
                                  rounded=True)
        self.create_label(popup.content, "Mot de passe réinitialisé.\n\nVotre nouveau mot de passe est :\n",
                               0, .2, 1, .8, halign="center", valign="middle")
        self.create_label(popup.content, "azerty", 0, .2, 1, .4, halign="center", valign="middle", bold=True)
        button = self.create_blue_button(popup.content, "OK", 0, 0, 1, .2)
        button.bind(on_release=self.controller.close_popup)
        popup = self.create_popup(self, pos_hint={"center_x": .5, "center_y": .4}, size_hint=(.55, .5),
                                  rounded=True)
        self.create_label(popup.content, "Veuillez rentrer votre identifiant :",
                               0, .2, 1, .8, halign="center", valign="middle")
        self.create_textInput(popup.content, 0, .4, 1, .15, font_size=14)
        button = self.create_blue_button(popup.content, "OK", 0, 0, .5, .2)
        button.bind(on_release=self.controller.reset_password)
        button = self.create_blue_button(popup.content, "CANCEL", .5, 0, .5, .2)
        button.bind(on_release=self.controller.close_password_reset)

    def resize_widgets(self, window=None, width=None, height=None):
        super().resize_widgets(window, width, height)
        self.create_rounded_line(self, .25, .175, .5, .4, (.4, .733, 1, 1))
