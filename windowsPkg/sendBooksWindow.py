from mainController import MainController
from mainView import MainView
from libraryPkg.library import Library
from libraryPkg.command import  Command, Commands
from kivy.uix.scrollview import ScrollView
from libraryPkg.book import Book
from kivy.uix.gridlayout import GridLayout

import datetime
import json


class SendBooksController(MainController):
    def __init__(self, mainWindow):
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = SendBooksView(self)
        self.commands = []
        self.library = Library()
        super().__init__(mainWindow, self.view)



    def send_command(self, instance):
        if self.view.textInputs[0].text != "" and self.view.textInputs[1].text != "":
            self.view.add_widget(self.view.popups[0], index=0)
        elif self.view.textInputs[0].text == "" and self.view.textInputs[1].text == "":
            self.view.add_widget(self.view.popups[2], index=0)

    def command_validated(self, instance):

        self.book = Book(None,None,0)
        self.book.author = self.view.textInputs[2].text
        self.book.title = self.view.textInputs[1].text
        self.book.code_barres = self.view.textInputs[4].text
        self.book.price = self.view.textInputs[3].text
        #On charge les commandes fournisseur déjà effectuées
        Commands.charger(self)

        #On crée la commande
        Commands.creercommand(self,int(self.view.textInputs[0].text),self.view.labels[2].text,self.book.code_barres,self.book.title,self.book.author,self.book.price,self.view.labels[6].text)
        #On sauvegarde les commandes dans le fichier json
        Commands.save_commands(self)
        self.view.do_layout()

        self.close_popup(instance)

        self.view.add_widget(self.view.popups[1], index=0)

        #On réénitialise les champs de saisie
        self.view.textInputs[0].text = ""
        self.view.textInputs[1].text = ""
        self.view.textInputs[4].text = ""
        self.view.textInputs[3].text = ""
        self.view.do_layout()


class SendBooksView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.create_labels()
        self.create_textInputs()
        self.create_buttons()
        self.create_popups()

    def create_labels(self):
        self.create_label(self, name="N° Commande :", x=.05, y=.35, width=0.5, height=.5, halign="left", font_size=20,
                          valign="top"),
        self.create_label(self, name="Nom Fournisseur :", x=.05, y=.28, width=.5, height=.5, halign="left",
                          valign="top", font_size=20),
        self.create_label(self, name="Fnac.com", x=.55, y=.28, width=.5, height=.5, halign="left", valign="top",
                          font_size=20)
        self.create_label(self, name="Titre Livre :", x=.18, y=.21, width=.5, height=.5, halign="left", valign="top",
                          font_size=20)
        self.create_label(self, name="Auteur :", x=.2, y=.14, width=.5, height=.5, halign="left", valign="top",
                          font_size=20)
        self.create_label(self, name="Date :", x=.25, y=.07, width=.5, height=.5, halign="left", valign="top",
                          font_size=20)
        self.create_label(self, name=str(self.date_now()), x=.55, y=.07, width=.5, height=.5, halign="left",
                          valign="top", font_size=20)
        self.create_label(self, name="Prix :", x=.25, y=.0, width=.5, height=.5, halign="left", valign="top",
                          font_size=20)
        self.create_label(self, name="ISBN :", x=.25, y=0.15, width=.5, height=.5, halign="left", valign="middle",
                          font_size=20)
    def create_textInputs(self):
        self.create_textInput(self, x=.4, y=.8, width=.5, height=.05)
        self.create_textInput(self, x=.4, y=.66, width=.5, height=.05, password=False)
        self.create_textInput(self, x=.4, y=.59, width=.5, height=.05, password=False)
        self.create_textInput(self, x=.4, y=.45, width=.5, height=.05, password=False)
        self.create_textInput(self, x=.4, y=.38, width=.5, height=.05, password=False)

    def create_buttons(self):
        button1 = self.create_button(self, name="Historique Commande", x=.1, y=.05, width=.5, height=.075)
        button1.id = "historicCommands"
        button1.bind(on_release=self.controller.change_view)

        button2 = self.create_blue_button(self, name="Envoyer", x=.25, y=.20, width=.5, height=.075)
        button2.bind(on_release=self.controller.send_command)


    def create_popups(self):
        popup1 = self.create_popup(self.view, title="Validation de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.75, .7), rounded=True)
        self.view.create_label(popup1.content, "Êtes-vous sûr de valider cette commande ?", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button1 = self.view.create_blue_button(popup1.content, "Oui", 0, 0, 0.4, .2)
        button2 = self.view.create_blue_button(popup1.content, "Non", 0.6, 0, 0.4, .2)
        button1.bind(on_release=self.controller.command_validated)
        button2.bind(on_release=self.controller.close_popup)

        popup2 = self.create_popup(self.view, title="Validation de Commande",pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup2.content, "Commande Créée !!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button3 = self.view.create_blue_button(popup2.content, "OK", 0, 0, 1, .2)
        button3.bind(on_release=self.controller.close_popup)

        popup3 = self.create_popup(self.view, title="Validation de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup3.content, "Veuillez entrer quelque chose !!!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button4 = self.view.create_blue_button(popup3.content, "OK", 0, 0, 1, .2)
        button4.bind(on_release=self.controller.close_popup)

        if self.controller.popup_shown is not None:
            self.controller.open_popup(self, self.controller.popup_shown)

    def date_now(self):
        date = datetime.datetime.now()
        day = date.day
        month = date.month
        year = date.year
        nowdate = str(day) + " / " + str(month) + " / " + str(year)
        return nowdate

    def clbk_envoyer(self, widget):
        self.controller.send_command(widget)


class HistoricCommandController(MainController):

    def __init__(self, mainWindow):
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = HistoricCommandView(self)
        self.commands = []
        super().__init__(mainWindow, self.view)

        Commands.charger(self)
        self.afficher_commandes()
        print("Commandes enregistrées: "+ str(len(self.commands)))

    def afficher_commandes(self):

        try:
            for command in self.commands:
                print("Lit les clés commandes")
                print(command.furnisher)
                self.view.layoutButton(str(command.furnisher)+" | "+str(command.date)+" | "+str(command.book))
                print(str(command.furnisher)+" | "+str(command.date)+" | "+str(command.book))
        except:
            print("L'affichage des commandes à échoué")

    @staticmethod
    def command_get_number(elem):
        return elem["num_command"]

    def command_details(self,bouton):
        Commands.charger(self)

        for i in range (len(self.view.contenu_scroll_grid.buttons)):

            if self.view.contenu_scroll_grid.buttons[i].uid == bouton.uid:
                for command in self.commands:
                    print("Commande: " + str(command.number))
                    if command.number == i+1:
                        popup = self.view.popups[0]
                        popup.content.labels[0].text = "N° de Commande : " + str(command.number)
                        popup.content.labels[1].text = "Fournisseur : " + str(command.furnisher)
                        popup.content.labels[2].text = "Titre du Livre : " + str(command.book)
                        popup.content.labels[3].text = "Auteur : " + str(command.author)
                        popup.content.labels[4].text = "Prix : " + str(command.price)
                        popup.content.labels[5].text = "Date : " + str(command.date)
                    else:
                        pass
                else:
                    print("Error commande "+str(i)+" non trouvée")

        self.view.add_widget(self.view.popups[0], index=0)

    def command_validated(self, instance):

        self.close_popup(instance)
        self.view.add_widget(self.view.popups[1], index=0)

class HistoricCommandView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)

        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.create_labels_v2()
        self.create_buttons_v2()
        self.create_popups_v2()
        self.grid=GridHistoric(pos_hint={'x':0.2, 'y':.0},size_hint_x=0.7,size_hint_y=0.8, row_default_height=15, cols=1,rows=10)

        self.contenu_scroll_grid = GridHistoric(pos_hint={'x':.5, 'y':.7},size_hint_x=0.8,size_hint_y=0.3, row_default_height=30, cols=1,padding=20)
        self.create_label(self.contenu_scroll_grid, name="TEST", x=0.3, y=.38, width=1, height=.5, halign="center",
                          font_size=30,
                          valign="top",array=True)
        self.contenu_scroll_grid.bind(minimum_height=self.contenu_scroll_grid.setter('height'))

        self.layout_scroll1()
        self.grid.add_widget(self.scroll_view)
        self.add_widget(self.grid)


    def create_labels_v2(self):
        self.create_label(self, name="Commandes Existantes", x=0.3, y=.38, width=0.5, height=.5, halign="center",
                          font_size=20,
                          valign="top")


    def create_buttons_v2(self):

        button3 = self.create_button(self, name="Retour menu", x=.6, y=.1, width=.3, height=.05,array=False)
        button3.id = "sendBooks"
        button3.bind(on_release=self.controller.change_view)

    def layout_scroll1(self):
        # Add the contend to the Scroll View
        self.scroll_view = ScrollView()
        self.scroll_view.add_widget(self.contenu_scroll_grid)

    def show_commands(self):

        for i, command in self.commands:
            if command is not None:
                self.show_command(command,i)

    def show_command(self,command,pos):

        if command is not None:
            while pos > len(self.commands):
                pos -= len(self.commands)

        command.bind(on_release=self.clbk_command_details)
        self.add_widget(command, index=1)

    def layoutButton(self,data):
        # Affiche le bouton avec un Numero commande / Date  / Prix
        btnuser = self.create_button(self.contenu_scroll_grid, name=data, x=.4, y=.6, width=.5, height=.075,array=True)
        btnuser.bind(on_release=self.clbk_command_details)

    def create_popups_v2(self):
        popup1 = self.create_popup(self, "Historique de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.6, .6), rounded=True,title_size=25)

        for i in range(6):
            self.create_label(popup1.content, "null", 0.1, 0.8 - i / 10, 0.8, .2,
                              halign="center", valign="middle", font_size=16)


        button1 = self.create_blue_button(popup1.content, "Oui", 0, 0, 0.4, .2)
        button2 = self.create_blue_button(popup1.content, "Non", 0.6, 0, 0.4, .2)
        button1.bind(on_release=self.controller.command_validated)
        button2.bind(on_release=self.controller.close_popup)

        popup2 = self.create_popup(self, title="Validation de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup2.content, "Commande Créée !!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button3 = self.view.create_blue_button(popup2.content, "OK", 0, 0, 1, .2)
        button3.bind(on_release=self.controller.close_popup)

        popup3 = self.create_popup(self, title="Validation de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup3.content, "Veuillez entrer quelque chose !!!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button4 = self.view.create_blue_button(popup3.content, "OK", 0, 0, 1, .2)
        button4.bind(on_release=self.controller.close_popup)

        if self.controller.popup_shown is not None:
            self.controller.open_popup(self, self.controller.popup_shown)


    def clbk_command_details(self,widget):
        self.controller.command_details(widget)

class GridHistoric(GridLayout):
    def __init__(self,**kwargs):
        GridLayout.__init__(self, **kwargs)

        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []




