from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup


class CustomPopup(Popup):
    def __init__(self, view, rounded=False, **kwargs):
        self.view = view
        self.title_align = "center"
        self.separator_height = 0
        self.background_color = (1, 1, 1, 1)
        if rounded:
            self.background = "images/PopupBgRounded.png"
        else:
            self.background = "images/PopupBg.png"
        self.title_color = (0, 0, 0, 1)
        super().__init__(**kwargs)
        self.book = None


class PopupContent(FloatLayout):
    def __init__(self, popup, **kwargs):
        self.popup = popup
        self.labels = []
        self.textInputs = []
        self.buttons = []
        super().__init__(**kwargs)
