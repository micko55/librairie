from mainController import MainController
from mainView import MainView
from libraryPkg.book import Book
from libraryPkg.command import Sale_Borrow, Sales_Borrows

from libraryPkg.library import Library
from kivy.uix.scrollview import ScrollView
from kivy.uix.gridlayout import GridLayout
import datetime
import json


class SalesController(MainController):
    def __init__(self, mainWindow):
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = SalesView(self)
        self.view2 = BorrowsView(self)
        super().__init__(mainWindow, self.view)
        self.sales = []
        self.borrows = []
        self.customers = []
        self.books = self.mainWindow.library.books
        self.check_disponibility()

    def check_disponibility(self):
        for book in self.view.library.books:
            test = False
            while test == False:
                if book.borrowed == False and book.sold == False:
                    self.view.labels[14].text = " OUI "
                    self.view.do_layout()
                    self.view2.labels[14].text = "OUI"
                    self.view2.do_layout()
                    test = True
                elif book.sold == True:
                    self.view.labels[14].text = " NON "
                    self.view.do_layout()
                    self.view2.labels[14].text = " NON"
                    self.view2.do_layout()
                    test = True
                elif book.borrowed == True:
                    self.view.labels[14].text = " INDISPONIBLE. RETOUR LE XX/XX/XXXX "
                    self.view.do_layout()
                    self.view2.labels[14].text = " INDISPONIBLE. RETOUR LE XX/XX/XXXX "
                    self.view2.do_layout()
                    test = True

    def create_customer(self):
        pass

    def send_sale(self, instance):
        if self.view.textInputs[0].text != "":
            if self.view.textInputs[5].text != "" and self.view.textInputs[6].text != "" and self.view.textInputs[7].text != "":
                self.view.add_widget(self.view.popups[0], index=0)
            elif self.view.textInputs[5].text == "" or self.view.textInputs[6].text == "" or self.view.textInputs[7].text == "":
                self.view.add_widget(self.view.popups[2], index=0)
        if self.view.textInputs[0].text == "":
            self.view.add_widget(self.view.popups[3], index=0)

    def send_borrow(self, instance):
        if self.view2.textInputs[0].text != "":
            if self.view2.textInputs[5].text != "" and self.view2.textInputs[6].text != "" and self.view2.textInputs[7].text != "":
                self.view2.add_widget(self.view2.popups[0], index=0)
            elif self.view2.textInputs[5].text == "" or self.view2.textInputs[6].text == "" or self.view2.textInputs[7].text != "":
                self.view2.add_widget(self.view2.popups[2], index=0)
        if self.view2.textInputs[0].text == "":
            self.view.add_widget(self.view2.popups[3], index=0)


    def sale_validated(self, instance):

        id_customer = self.view.textInputs[0].text
        date = self.view.labels[6].text
        storage = self.view.textInputs[5].text
        shelve = self.view.textInputs[6].text
        book_number = self.view.textInputs[7].text

        self.check_disponibility()
        #On attribue la valeur sold=True à book
        for book in self.books:
            if book.storage.name == storage and book.shelf.index == shelve and book.number == book_number:
                    book.sold = True
                    print("Livre : " +storage+shelve+" - "+str(book_number)+"Vendu")
                    print("Livre : " + book.storage.name + book.shelf.index + " - " + str(book.number) + "Mis de la bbd à "+str(book.sold))
            else:
                print("ça beugue couillon")
            #except:
                #print("Le livre à l'emplacement: "+str(storage+str(shelve)+" - "+str(book_number)+" n'existe plus"))
                #pass

        statusB = None
        statusS = True

        self.sale = Sale_Borrow(id_customer,date,storage,shelve,book_number,statusS,statusB)

        # On charge les ventes déjà effectuées

        Sales_Borrows.load_sales(self)
        Sales_Borrows.create_sale(self,id_customer, date, storage, shelve, book_number,statusS,statusB)
        Sales_Borrows.save_sales(self)
        self.mainWindow.library.save_books()


        self.close_popup(instance)

        self.view.add_widget(self.view.popups[1], index=0)
        self.view.textInputs[0].text = ""
        self.view.textInputs[1].text = ""
        self.view.textInputs[0].text = ""
        self.view.textInputs[1].text = ""
        self.view.textInputs[2].text = ""
        self.view.textInputs[3].text = ""
        self.view.textInputs[4].text = ""
        self.view.textInputs[5].text = ""
        self.view.textInputs[6].text = ""
        self.view.textInputs[7].text = ""

    def borrow_validated(self, instance):


        id_customer = self.view2.textInputs[0].text
        date = self.view2.labels[6].text
        storage = self.view2.textInputs[5].text
        shelve = self.view2.textInputs[6].text
        book_number = self.view2.textInputs[7].text

        self.check_disponibility()
        #On attribue la valeur borrowed=true à book
        for book in self.books:
            try:
                if book[storage, shelve, book_number].borrowed == False:
                    book[storage, shelve, book_number].borrowed = True
                    print("Livre: " + storage + shelve + " - " + str(book_number) + "Emprunté")
            except:
                print("Le livre à l'emplacement: " + str(
                    storage + str(shelve) + " - " + str(book_number) + " n'existe plus"))
                pass
        statusB = True
        statusS = None

        self.borrow = Sale_Borrow(id_customer, date, storage, shelve, book_number,statusS,statusB)
        #On charge les emprunts déjà effectués
        Sales_Borrows.load_borrows(self)
        Sales_Borrows.create_borrow(self,id_customer, date, storage, shelve, book_number,statusS,statusB)
        Sales_Borrows.save_borrows(self)

        self.close_popup(instance)

        self.view2.add_widget(self.view2.popups[1], index=0)
        self.view2.textInputs[0].text = ""
        self.view2.textInputs[1].text = ""
        self.view2.textInputs[2].text = ""
        self.view2.textInputs[3].text = ""
        self.view2.textInputs[4].text = ""
        self.view2.textInputs[5].text = ""
        self.view2.textInputs[6].text = ""
        self.view2.textInputs[7].text = ""



class SalesView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.filter = ""
        self.sorted_by = ""
        self.search_text = ""
        self.library = self.controller.mainWindow.library
        self.current_books = self.library.books
        self.current_storage = self.library.storages[0]
        self.current_shelf = self.current_storage.shelves[0]
        self.shown_books = self.current_storage.shelves[0].books + self.current_storage.shelves[1].books + \
                           self.current_storage.shelves[2].books
        self.create_labels()
        self.create_textInputs()
        self.create_buttons()
        self.create_popups()

    def create_labels(self):
        self.create_label(self, name="Id Client :", x=.18, y=.3, width=0.5, height=.5, halign="left", font_size=15,
                          valign="top"),
        self.create_label(self, name="Nom Client :", x=.17, y=.23, width=.5, height=.5, halign="left",
                          valign="top", font_size=15),
        self.create_label(self, name="Nom client", x=.50, y=.23, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name="Titre Ouvrage :", x=.15, y=.16, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name="Auteur :", x=.22, y=.1, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name="Date :", x=.26, y=.05, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        #Label 6 Date
        self.create_label(self, name=str(self.date_now()), x=.55, y=.05, width=.5, height=.5, halign="left",
                          valign="top", font_size=15)
        self.create_label(self, name="Prix :", x=.27, y=.0, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name="ISBN :", x=.25, y=0.18, width=.5, height=.5, halign="left", valign="middle",
                          font_size=15)
        self.create_label(self, name="Emplacement :", x=.13, y=0.12, width=.5, height=.5, halign="left", valign="center",
                          font_size=15)
        self.create_label(self, name=" - ", x=.57, y=0.12, width=.5, height=.5, halign="left",
                          valign="center",
                          font_size=15)
        self.create_label(self, name=" - ", x=.77, y=0.12, width=.5, height=.5, halign="left",
                          valign="center",
                          font_size=15)
        #Label Nouvelle vente Haut gauche
        self.create_label(self, name="Nouvelle Vente :", x=.1, y=0.38, width=.5, height=.5, halign="left",
                          valign="top",
                          font_size=18)
        #Labels Disponibilité
        self.create_label(self, name="Disponibilité :", x=.18, y=.05, width=.5, height=.5, halign="left", valign="center",
                          font_size=15)
        self.create_label(self, name="Oui / Non / Pas avant le xx", x=.48, y=.05, width=.5, height=.5, halign="left",
                          valign="center", font_size=15)

    def create_textInputs(self):
        # TextInput Id Client
        self.create_textInput(self, x=.4, y=.77, width=.4, height=.04,font_size=8)
        # TextInput Titre Livre
        self.create_textInput(self, x=.4, y=.63, width=.4, height=.04, font_size=8)
        # TextInput Auteur
        self.create_textInput(self, x=.4, y=.56, width=.4, height=.04, font_size=8)
        # TextInput Prix
        self.create_textInput(self, x=.4, y=.47, width=.4, height=.04, font_size=8)
        # TextInput N° ISBN
        self.create_textInput(self, x=.4, y=.41, width=.4, height=.04, font_size=8)
        # TextInput N° Armoire
        self.create_textInput(self, x=.4, y=.35, width=.15, height=.04, font_size=8)
        # TextInput N° Etagère
        self.create_textInput(self, x=.6, y=.35, width=.15, height=.04, font_size=8)
        # TextInput N° Livre sur étagère
        self.create_textInput(self, x=.8, y=.35, width=.15, height=.04, font_size=8)

    def create_buttons(self):
        button1 = self.create_button(self, name="Nouvel Emprunt", x=.65, y=.84, width=.3, height=.05)
        button1.id = "borrows"
        button1.bind(on_release=self.controller.change_view)
        self.buttons.append(button1)
        button2 = self.create_blue_button(self, name="Valider", x=.35, y=.20, width=.3, height=.05)
        button2.bind(on_release=self.controller.send_sale)
        self.buttons.append(button2)
        button3 = self.create_button(self, name="Historique Ventes", x=.1, y=.1, width=.5, height=.075)
        button3.id = "historicSales"
        button3.bind(on_release=self.controller.change_view)
        self.buttons.append(button3)

    def create_popups(self):
        popup1 = self.create_popup(self.view, title="Validation de Vente", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.6, .6), rounded=True)
        self.view.create_label(popup1.content, "Êtes-vous sûr de valider cette vente ?", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button1 = self.view.create_blue_button(popup1.content, "Oui", 0, 0, 0.4, .2)
        button2 = self.view.create_blue_button(popup1.content, "Non", 0.6, 0, 0.4, .2)
        button1.bind(on_release=self.controller.sale_validated)
        button2.bind(on_release=self.controller.close_popup)

        popup2 = self.create_popup(self.view, title="Validation de Vente",pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup2.content, "Vente Créée !!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button3 = self.view.create_blue_button(popup2.content, "OK", 0, 0, 1, .2)
        button3.bind(on_release=self.controller.close_popup)

        popup3 = self.create_popup(self.view, title="Validation de Vente", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup3.content, "Veuillez au moins rentrer l'emplacement d'un livre", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button4 = self.view.create_blue_button(popup3.content, "OK", 0, 0, 1, .2)
        button4.bind(on_release=self.controller.close_popup)

        popup4 = self.create_popup(self.view, title="Identification Client", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.6, .6), rounded=True)
        self.view.create_label(popup4.content, "Id Client Non rentré: Voulez-vous créer un client ?", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button5 = self.view.create_blue_button(popup4.content, "Oui", 0, 0, 0.4, .2)
        button6 = self.view.create_blue_button(popup4.content, "Non", 0.6, 0, 0.4, .2)
        button5.bind(on_release=self.controller.create_customer)
        button6.bind(on_release=self.controller.close_popup)

        if self.controller.popup_shown is not None:
            self.controller.open_popup(self, self.controller.popup_shown)


    def date_now(self):

        date = datetime.datetime.now()
        day = date.day
        month = date.month
        year = date.year
        nowdate = str(day) + " / " + str(month) + " / " + str(year)
        return nowdate




class BorrowsView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.filter = ""
        self.sorted_by = ""
        self.search_text = ""
        self.library = self.controller.mainWindow.library
        self.current_books = self.library.books
        self.current_storage = self.library.storages[0]
        self.current_shelf = self.current_storage.shelves[0]
        self.shown_books = self.current_storage.shelves[0].books + self.current_storage.shelves[1].books + \
                           self.current_storage.shelves[2].books

        self.create_labels2()
        self.create_textInputs2()
        self.create_buttons2()
        self.create_popups2()

    def create_labels2(self):
        self.create_label(self, name="Id Client :", x=.18, y=.3, width=0.5, height=.5, halign="left", font_size=15,
                          valign="top"),
        self.create_label(self, name="Nom Client :", x=.17, y=.23, width=.5, height=.5, halign="left",
                          valign="top", font_size=15),
        self.create_label(self, name="Nom client", x=.50, y=.23, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name="Titre Ouvrage :", x=.15, y=.16, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name="Auteur :", x=.22, y=.1, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name="Date :", x=.26, y=.05, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name=str(self.date_now()), x=.55, y=.05, width=.5, height=.5, halign="left",
                          valign="top", font_size=15)
        self.create_label(self, name="Prix :", x=.27, y=.0, width=.5, height=.5, halign="left", valign="top",
                          font_size=15)
        self.create_label(self, name="ISBN :", x=.25, y=0.18, width=.5, height=.5, halign="left", valign="middle",
                          font_size=15)
        self.create_label(self, name="Emplacement :", x=.13, y=0.12, width=.5, height=.5, halign="left", valign="center",
                          font_size=15)
        self.create_label(self, name=" - ", x=.57, y=0.12, width=.5, height=.5, halign="left",
                          valign="center",
                          font_size=15)
        self.create_label(self, name=" - ", x=.77, y=0.12, width=.5, height=.5, halign="left",
                          valign="center",
                          font_size=15)
        # Label Nouvelle emprunt Haut gauche
        self.create_label(self, name="Nouvel Emprunt :", x=.1, y=0.38, width=.5, height=.5, halign="left",
                          valign="top",
                          font_size=18)
        # Labels Disponibilité
        self.create_label(self, name="Disponibilité :", x=.18, y=.05, width=.5, height=.5, halign="left",
                          valign="center",
                          font_size=15)
        self.create_label(self, name="Oui / Non / Pas avant le xx", x=.48, y=.05, width=.5, height=.5, halign="left",
                          valign="center", font_size=15)

    def create_textInputs2(self):
        # TextInput N° Vente
        self.create_textInput(self, x=.4, y=.77, width=.4, height=.04, font_size=8)
        # TextInput Titre Livre
        self.create_textInput(self, x=.4, y=.63, width=.4, height=.04, font_size=8)
        # TextInput Auteur
        self.create_textInput(self, x=.4, y=.56, width=.4, height=.04, font_size=8)
        # TextInput Prix
        self.create_textInput(self, x=.4, y=.47, width=.4, height=.04, font_size=8)
        # TextInput N° ISBN
        self.create_textInput(self, x=.4, y=.41, width=.4, height=.04, font_size=8)
        # TextInput N° Armoire
        self.create_textInput(self, x=.4, y=.35, width=.15, height=.04, font_size=8)
        # TextInput N° Etagère
        self.create_textInput(self, x=.6, y=.35, width=.15, height=.04, font_size=8)
        # TextInput N° Livre sur étagère
        self.create_textInput(self, x=.8, y=.35, width=.15, height=.04, font_size=8)

    def create_buttons2(self):
        button1 = self.create_button(self, name="Nouvelle Vente", x=.65, y=.84, width=.3, height=.05)
        button1.id = "sales"
        button1.bind(on_release=self.controller.change_view)
        self.buttons.append(button1)
        button2 = self.create_blue_button(self, name="Valider", x=.35, y=.20, width=.3, height=.05)
        button2.bind(on_release=self.controller.send_borrow)
        self.buttons.append(button2)
        button3 = self.create_button(self, name="Historique Emprunts", x=.1, y=.1, width=.5, height=.075)
        button3.id = "historicBorrows"
        button3.bind(on_release=self.controller.change_view)
        self.buttons.append(button3)

    def create_popups2(self):
        popup1 = self.create_popup(self.view, title="Validation d'emprunt'", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.6, .6), rounded=True)
        self.view.create_label(popup1.content, "Êtes-vous sûr de valider cet emprunt ?", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button1 = self.view.create_blue_button(popup1.content, "Oui", 0, 0, 0.4, .2)
        button2 = self.view.create_blue_button(popup1.content, "Non", 0.6, 0, 0.4, .2)
        button1.bind(on_release=self.controller.borrow_validated)
        button2.bind(on_release=self.controller.close_popup)

        popup2 = self.create_popup(self.view, title="Validation d'emprunt",pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup2.content, "Emprunt créé !!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button3 = self.view.create_blue_button(popup2.content, "OK", 0, 0, 1, .2)
        button3.bind(on_release=self.controller.close_popup)

        popup3 = self.create_popup(self.view, title="Validation d'emprunt", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup3.content, "Veuillez entrer quelque chose !!!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button4 = self.view.create_blue_button(popup3.content, "OK", 0, 0, 1, .2)
        button4.bind(on_release=self.controller.close_popup)

        popup4 = self.create_popup(self.view, title="Identification Client", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.6, .6), rounded=True)
        self.view.create_label(popup4.content, "Id Client Non rentré: Voulez-vous créer un client ?", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button5 = self.view.create_blue_button(popup4.content, "Oui", 0, 0, 0.4, .2)
        button6 = self.view.create_blue_button(popup4.content, "Non", 0.6, 0, 0.4, .2)
        button5.bind(on_release=self.controller.create_customer)
        button6.bind(on_release=self.controller.close_popup)

        if self.controller.popup_shown is not None:
            self.controller.open_popup(self, self.controller.popup_shown)


    def date_now(self):
        date = datetime.datetime.now()
        day = date.day
        month = date.month
        year = date.year
        nowdate = str(day) + " / " + str(month) + " / " + str(year)
        return nowdate

    def clbk_borrow_historic(self, widget):
        self.controller.borrow_historic(widget)
        print("Affichage des détails des emprunts non effectué pour le moment: Bdd non crée")

class HistoricSBController(MainController):

    def __init__(self, mainWindow):
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = HistoricSalesView(self)
        self.view2 = HistoricBorrowsView(self)
        self.sales = []
        self.borrows = []
        super().__init__(mainWindow, self.view)

    def afficher_sales(self):
        Sales_Borrows.load_sales()
        for sale in self.sales:
            self.view.layoutButton(str(sale.id_client)+" | "+str(sale.date)+" | "+str(sale.book.title))
            print(str(sale.id_client)+" | "+str(sale.date)+" | "+str(sale.book.title))
        print(self.sales)

    def afficher_borrows(self):
        Sales_Borrows.load_borrows()
        for borrow in self.borrows:
            self.view.layoutButton(str(borrow.id_client)+" | "+str(borrow.date)+" | "+str(borrow.book.title))
            print(str(borrow.id_client)+" | "+str(borrow.date)+" | "+str(borrow.book.title))
        print(self.borrows)


    @staticmethod
    def command_get_number(elem):
        return elem["num_command"]

    def sale_details(self,bouton):
        Sales_Borrows.load_sales()

        for i in range(len(self.view.contenu_scroll_grid.buttons)):
            if self.view.contenu_scroll_grid.buttons[i].uid == bouton.uid:
                for data in self.sales:
                    print("Vente: " + str(data.number))
                    if data.number == i + 1:

                        popup = self.view.popups[0]
                        popup.content.labels[0].text = "Client : " + str(data.id_client)
                        popup.content.labels[1].text = "Emplacement : " + str(data.book.storage)+str(data.book.shelf)+" - "+str(data.book.number)
                        popup.content.labels[2].text = "Titre du Livre : " + str(data.book.title)
                        popup.content.labels[3].text = "Auteur : " + str(data.book.author)
                        popup.content.labels[4].text = "Prix : " + str(data.book.price)
                        popup.content.labels[5].text = "Date : " + str(data.date)
                    else:
                        pass
            else:
                print("Error Vente "+str(i)+" non trouvée")
        self.view.add_widget(self.view.popups[0], index=0)

    def borrow_details(self,bouton):
        Sales_Borrows.load_borrows()

        for i in range(len(self.view2.contenu_scroll_grid.buttons)):
            if self.view2.contenu_scroll_grid.buttons[i].uid == bouton.uid:
                for data in self.borrows:
                    print("Emprunt: " + str(data.number))
                    if data.number == i + 1:
                        popup = self.view2.popups[0]
                        popup.content.labels[0].text = "Client : " + str(data.id_client)
                        popup.content.labels[1].text = "Emplacement : " + str(data.book.storage) + str(data.book.shelf) + " - " + str(data.book.number)
                        popup.content.labels[2].text = "Titre du Livre : " + str(data.book.title)
                        popup.content.labels[3].text = "Auteur : " + str(data.book.author)
                        popup.content.labels[4].text = "Prix : " + str(data.book.price)
                        popup.content.labels[5].text = "Date : " + str(data.date)
                        popup.content.labels[6].text = "Temps avant restitution restant : "+ str(data.book.return_time_left)

        self.view.add_widget(self.view2.popups[0], index=0)

    def details_number(self,widget):
        pass

    def command_validated(self, instance):

        self.close_popup(instance)
        self.view.add_widget(self.view.popups[1], index=0)

class HistoricSalesView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)

        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.create_labels_v2()
        self.create_buttons_v2()
        self.create_popups_v2()
        self.grid=GridLayout(pos_hint={'x':0.2, 'y':.0},size_hint_x=0.7,size_hint_y=0.8, row_default_height=15, cols=1,rows=10)
        self.contenu_scroll_grid = GridLayout(pos_hint={'x':.5, 'y':.7},size_hint_x=0.8,size_hint_y=0.3, row_default_height=30, cols=1,padding=20)
        self.create_label(self.contenu_scroll_grid, name="TEST", x=0.3, y=.38, width=1, height=.5, halign="center",
                          font_size=30,
                          valign="top",array=False)
        self.contenu_scroll_grid.bind(minimum_height=self.contenu_scroll_grid.setter('height'))

        self.layout_scroll1()
        self.grid.add_widget(self.scroll_view)
        self.add_widget(self.grid)


    def create_labels_v2(self):
        self.create_label(self, name="Ventes Existantes", x=0.3, y=.38, width=0.5, height=.5, halign="center",
                          font_size=20,
                          valign="top")


    def create_buttons_v2(self):

        button3 = self.create_button(self, name="Retour menu Ventes", x=.6, y=.1, width=.3, height=.05,array=False)
        button3.id = "sales"
        button3.bind(on_release=self.controller.change_view)

    def layout_scroll1(self):
        # Add the contend to the Scroll View
        self.scroll_view = ScrollView()
        self.scroll_view.add_widget(self.contenu_scroll_grid)

    def show_sales(self):

        for i, sale in self.sales:
            if sale is not None:
                self.show_command(sale,i)

    def show_command(self,command,pos):

        if command is not None:
            while pos > len(self.commands):
                pos -= len(self.commands)

        command.bind(on_release=self.clbk_command_details)
        self.add_widget(command, index=1)

    def layoutButton(self,data):
        # Affiche le bouton avec un Numero commande / Date  / Prix
        btnuser = self.create_button(self.contenu_scroll_grid, name=data, x=.4, y=.6, width=.5, height=.075,array=False)
        btnuser.bind(on_release=self.clbk_sale_details)

    def create_popups_v2(self):
        popup1 = self.create_popup(self, "Historique de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.6, .6), rounded=True,title_size=25)

        for i in range(6):
            self.create_label(popup1.content, "null", 0.1, 0.8 - i / 10, 0.8, .2,
                              halign="center", valign="middle", font_size=16)


        button1 = self.create_blue_button(popup1.content, "Oui", 0, 0, 0.4, .2)
        button2 = self.create_blue_button(popup1.content, "Non", 0.6, 0, 0.4, .2)
        button1.bind(on_release=self.controller.command_validated)
        button2.bind(on_release=self.controller.close_popup)

        popup2 = self.create_popup(self, title="Validation de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup2.content, "Commande Créée !!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button3 = self.view.create_blue_button(popup2.content, "OK", 0, 0, 1, .2)
        button3.bind(on_release=self.controller.close_popup)

        popup3 = self.create_popup(self, title="Validation de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup3.content, "Veuillez entrer quelque chose !!!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button4 = self.view.create_blue_button(popup3.content, "OK", 0, 0, 1, .2)
        button4.bind(on_release=self.controller.close_popup)

        if self.controller.popup_shown is not None:
            self.controller.open_popup(self, self.controller.popup_shown)


    def clbk_sale_details(self,widget):
        self.controller.sale_details(widget)

class HistoricBorrowsView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)

        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.create_labels_v2()
        self.create_buttons_v2()
        self.create_popups_v2()
        self.grid=GridLayout(pos_hint={'x':0.2, 'y':.0},size_hint_x=0.7,size_hint_y=0.8, row_default_height=15, cols=1,rows=10)
        self.contenu_scroll_grid = GridLayout(pos_hint={'x':.5, 'y':.7},size_hint_x=0.8,size_hint_y=0.3, row_default_height=30, cols=1,padding=20)
        self.create_label(self.contenu_scroll_grid, name="TEST", x=0.3, y=.38, width=1, height=.5, halign="center",
                          font_size=30,
                          valign="top",array=False)
        self.contenu_scroll_grid.bind(minimum_height=self.contenu_scroll_grid.setter('height'))

        self.layout_scroll1()
        self.grid.add_widget(self.scroll_view)
        self.add_widget(self.grid)


    def create_labels_v2(self):
        self.create_label(self, name="Emprunts Existants", x=0.3, y=.38, width=0.5, height=.5, halign="center",
                          font_size=20,
                          valign="top")


    def create_buttons_v2(self):

        button3 = self.create_button(self, name="Retour menu Emprunts", x=.6, y=.1, width=.3, height=.05,array=False)
        button3.id = "borrows"
        button3.bind(on_release=self.controller.change_view)

    def layout_scroll1(self):
        # Add the contend to the Scroll View
        self.scroll_view = ScrollView()
        self.scroll_view.add_widget(self.contenu_scroll_grid)

    def show_borrows(self):

        for i, borrow in self.borrows:
            if borrow is not None:
                self.show_command(borrow,i)

    def show_command(self,command,pos):

        if command is not None:
            while pos > len(self.commands):
                pos -= len(self.commands)

        command.bind(on_release=self.clbk_command_details)
        self.add_widget(command, index=1)

    def layoutButton(self,data):
        # Affiche le bouton avec un Numero commande / Date  / Prix
        btnuser = self.create_button(self.contenu_scroll_grid, name=data, x=.4, y=.6, width=.5, height=.075,array=False)
        btnuser.bind(on_release=self.clbk_borrow_details)

    def create_popups_v2(self):
        popup1 = self.create_popup(self, "Historique d'emprunts'", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.6, .6), rounded=True,title_size=25)

        for i in range(6):
            self.create_label(popup1.content, "null", 0.1, 0.8 - i / 10, 0.8, .2,
                              halign="center", valign="middle", font_size=16)


        button1 = self.create_blue_button(popup1.content, "Oui", 0, 0, 0.4, .2)
        button2 = self.create_blue_button(popup1.content, "Non", 0.6, 0, 0.4, .2)
        button1.bind(on_release=self.controller.command_validated)
        button2.bind(on_release=self.controller.close_popup)

        popup2 = self.create_popup(self, title="Validation de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup2.content, "Commande Créée !!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button3 = self.view.create_blue_button(popup2.content, "OK", 0, 0, 1, .2)
        button3.bind(on_release=self.controller.close_popup)

        popup3 = self.create_popup(self, title="Validation de Commande", pos_hint={"center_x": .5, "center_y": .4},
                                   size_hint=(.55, .6), rounded=True)
        self.view.create_label(popup3.content, "Veuillez entrer quelque chose !!!", 0, .2, 1, .8,
                               halign="center", valign="middle")
        button4 = self.view.create_blue_button(popup3.content, "OK", 0, 0, 1, .2)
        button4.bind(on_release=self.controller.close_popup)

        if self.controller.popup_shown is not None:
            self.controller.open_popup(self, self.controller.popup_shown)


    def clbk_borrow_details(self,widget):
        self.controller.borrow_details(widget)