import datetime

from kivy.graphics import Color, Line
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.scrollview import ScrollView

from mainController import MainController
from mainView import MainView


# ACCOUNT
class AccountMainView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self._image_id = 1
        self._profile_image = Image(source="images/" + str(self._image_id) + ".jpg")
        self.create_labels()
        self.create_textInputs()
        self.create_buttons()

    def show_rectangle(self, view):
        rectangle = view.canvas.before
        rectangle.add(Color(0, 0, 0, 1))
        rectangle.add(
            Line(rectangle=(0.5 * self.width, 0.5 * self.height, 0.5 * self.width, 0.5 * self.height)))
        self.view.add_widget(rectangle)

    def create_labels(self):
        self.create_label(self, name="Mon Compte", x=0, y=.7, width=.9, height=.1, halign="center", valign="top",
                          font_size=16)
        self.create_label(self, name="Nom", x=0.1, y=.65, width=.7, height=.1, halign="center",
                          valign="top", font_size=10)
        self.create_label(self, name="Adresse", x=0.1, y=.6, width=.7, height=.1, halign="center", valign="top",
                          font_size=10)
        self.create_label(self, name="Mail", x=0.1, y=.55, width=.7, height=.1, halign="center",
                          valign="top", font_size=10)
        self.show_image()

    def create_textInputs(self):
        self.create_textInput(self, x=.5, y=.4, width=.45, height=.05, password=True)

    def create_buttons(self):
        mdp = self.create_button(self, name="Changer de mot de passe", x=.1, y=.4, width=.4, height=.075)
        # mdp.bind(on_release=)
        deco = self.create_button(self, name="Déconnexion", x=.1, y=.3, width=.4, height=.075)
        deco.id = "login"
        deco.bind(on_release=self.controller.change_view)
        budget = self.create_button(self, name="Suivre Budget", x=.1, y=.1, width=.4, height=.075)
        budget.id = "budget"
        budget.bind(on_release=self.controller.change_view)
        manage = self.create_button(self, name="Management", x=.5, y=.1, width=.4, height=.075)
        manage.id = "management"
        manage.bind(on_release=self.controller.change_view)

    def show_image(self):
        image = self._profile_image
        image.size_hint = (0.25, 0.25)
        image.pos_hint = {'top': 0.85}
        self.add_widget(image)


class AccountController(MainController):
    def __init__(self, mainWindow):
        super().__init__(mainWindow, self)
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = AccountMainView(self)
        self.old_password = str
        self.new_password = str

    def change_password(self, old_pw: str, new_pw: str):
        pass

    def disconnect(self):
        pass

    def add_employee(self, mail: str, password: str):
        pass

    def change_employee_role(self, employee, new_role: int):
        pass

    def show_popup(self):
        pass

    def show_graph(self):
        pass


# MANAGEMENT
class AccountManagementView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.create_labels_management()
        self.create_textInputs_management()
        self.create_buttons_management()
        self.grid = GridLayout(pos_hint={'x': 0.2, 'y': .0}, size_hint_x=0.7, size_hint_y=0.8, row_default_height=15,
                               cols=1, rows=10)
        self.contenu_scroll_grid = GridLayout(pos_hint={'x': .5, 'y': .7}, size_hint_x=0.8, size_hint_y=0.3,
                                              row_default_height=30, cols=1, padding=20)
        self.create_label(self.view, name="TEST", x=0.3, y=.38, width=1, height=.5, halign="center",
                          font_size=30,
                          valign="top", array=False)
        self.contenu_scroll_grid.bind(minimum_height=self.contenu_scroll_grid.setter('height'))
        self.scroll_view = ScrollView()
        self.layout_scroll1()
        self.grid.add_widget(self.scroll_view)
        self.add_widget(self.grid)

    def create_buttons_management(self):
        recruit = self.create_button(self, name="Recruter", x=.1, y=.4, width=.4, height=.075)
        back = self.create_button(self, name="Retour Compte", x=.1, y=.3, width=.4, height=.075)
        back.id = "account"
        back.bind(on_release=self.controller.change_view)

    def create_labels_management(self):
        self.create_label(self, name="Management :", x=.05, y=.35, width=0.5, height=.5, halign="left", font_size=20,
                          valign="top")

    def create_textInputs_management(self):
        pass

    def layout_scroll1(self):
        # Add the contend to the Scroll View
        self.scroll_view.add_widget(self.contenu_scroll_grid)


class AccountManagementController(MainController):
    def __init__(self, mainWindow):
        super().__init__(mainWindow, self)
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = AccountManagementView(self)
        self.old_password = str
        self.new_password = str


# BUDGET
class AccountBudgetView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.current_date = self.date_now()
        self.create_labels_budget()
        self.create_buttons_budget()

    def create_buttons_budget(self):
        back = self.create_button(self, name="Retour Compte", x=.1, y=.3, width=.4, height=.075)
        back.id = "account"
        back.bind(on_release=self.controller.change_view)

    @staticmethod
    def date_now():
        date = datetime.datetime.now()
        day = date.day
        month = date.month
        year = date.year
        nowdate = str(day) + " / " + str(month) + " / " + str(year)
        return nowdate

    def create_labels_budget(self):
        self.create_label(self, name="Suivre Budget :", x=.05, y=.35, width=0.5, height=.5, halign="left", font_size=20,
                          valign="top")
        self.create_label(self, name=f"Chiffre d'affaires le {self.current_date}", x=.05, y=.35, width=0.5, height=.5,
                          halign="left", font_size=20,
                          valign="top")


class AccountBudgetController(MainController):
    def __init__(self, mainWindow):
        super().__init__(mainWindow, self)
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = AccountBudgetView(self)
