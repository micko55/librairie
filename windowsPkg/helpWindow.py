from mainController import MainController
from mainView import MainView


class HelpController(MainController):
    def __init__(self, mainWindow):
        self.popup_shown = None
        self.mainWindow = mainWindow
        self.view = HelpView(self)
        super().__init__(mainWindow, self.view)


class HelpView(MainView):
    def __init__(self, controller, **kwargs):
        super().__init__(controller, self, **kwargs)
        self.labels = []
        self.textInputs = []
        self.buttons = []
        self.popups = []
        self.create_labels()
        self.create_textInputs()
        self.create_buttons()
        self.create_popups()
