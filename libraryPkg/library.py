import csv
import json

from kivy.graphics import Rotate
from kivy.uix.label import Label

from libraryPkg.storage import Storage
from libraryPkg.shelf import Shelf
from libraryPkg.book import Book
from libraryPkg.command import Sales_Borrows
from libraryPkg.command import Commands



class Library():
    def __init__(self):
        self.storages = []
        self.books = []
        self.borrows = Sales_Borrows()
        self.sales = Sales_Borrows()
        self.create_storages()
        self.create_shelves()
        self.create_books()
        self.sales.load_sales()
        self.borrows.load_borrows()
        self.check_borrowed_sold_status()
        self.save_books()

    def save_books(self):
        data = {}
        i = 0
        for book in self.books:
            data[i] = book.get_data()
            print(book.shelf.index)
            i += 1
        with open("library_books.json", 'w') as f:
            json.dump(data, f, indent=11)
        f.close()

    def check_borrowed_sold_status(self):
        books= self.books
        for borrow in self.borrows.borrows:
            if borrow.book_borrowed == True:
                for book in books:
                    if book.storage.name == borrow.book.storage and book.shelf.index == borrow.shelve and book.number == borrow.book_number:
                        book.borrowed = True
                    else:
                        print("N'a rien trouvé à cet emplacement")
                        print(book.storage.name)
                        print(borrow.book.storage)
                        print(book.shelf.index)
                        print(borrow.shelve)
                        print(str(book.number))
                        print(str(borrow.book_number))

                        pass
            else:
                pass

        for sale in self.sales.sales:
            if sale.book_sold == True:
                for book in books:
                    if book.storage.name == sale.book.storage and book.shelf.index == sale.shelve and book.number == sale.book_number:
                        book.sold = True
                    else:
                        print("N'a rien trouvé à cet emplacement")
                        print(book.storage.name)
                        print(sale.book.storage)
                        print(book.shelf.index)
                        print(sale.shelve)
                        print(str(book.number))
                        print(str(sale.book_number))
                        pass

    def create_storages(self):
        letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
        for i in range(10):
            self.storages.append(Storage(self, i))
            self.storages[i].name = letters[i]

    def create_shelves(self):
        for storage in self.storages:
            for i in range(10):
                storage.shelves.append(Shelf(storage, i))

    def create_books(self):
        books = []
        with open("input.csv", "r", encoding="utf-8") as f:
            csvDict = csv.DictReader(f, delimiter=";")
            for book in csvDict:
                books.append(dict(book))
        self.original_sort_by(books, "name")
        self.original_sort_by(books, "author")
        n = 0
        for storage in self.storages:
            for shelf in storage.shelves:
                for i in range(20):
                    if n >= len(books):
                        shelf.books.append(None)
                    else:
                        book = Book(storage, shelf, i, source="images/book.png", size=(151, 41), size_hint=(None, None),
                                    author=books[n]["Auteur"], title=books[n]["Livre"], genre=books[n]["Genre"],
                                    price=books[n]["Prix"], code_barres=books[n]["Code-barres"], rotation=90,
                                    auto_bring_to_front=False)
                        self.books.append(book)
                        shelf.books.append(book)
                    n += 1

    def find_book(self, storage, shelf, index):
        return self.storages[storage].shelves[shelf].books[index]

    def search(self, value: str):
        books = []
        value = value.lower()
        for book in self.books:
            if book is not None:
                if value in book.title.lower() or value in book.author.lower() or value in book.genre.lower() or value \
                        in str(book.price).lower() or value in str(book.code_barres).lower():
                    books.append(book)
        return books

    @staticmethod
    def search_in(searchedBooks, kind: str, value: str):
        value = value.lower()
        books = []
        allBooks = searchedBooks.copy()
        for book in searchedBooks:
            if book is not None:
                if ((kind == "title" and value in book.title.lower()) or
                        (kind == "author" and value in book.author.lower()) or
                        (kind == "genre" and value in book.genre.lower()) or
                        (kind == "price" and value in str(book.price).lower()) or
                        (kind == "code-barres" and value in str(book.code_barres)) or
                        (kind == "storage" and value == book.storage.name.lower()) or
                        (kind == "shelf" and value == str(book.shelf.index))):
                    books.append(book)
        return books

    def filter_by(self, tList, value: str):
        pass

    def sort_by(self, tList, value: str, reverse=False):
        if value == "author":
            tList.sort(key=self.book_get_author, reverse=reverse)
        elif value == "title":
            tList.sort(key=self.book_get_name, reverse=reverse)
        elif value == "genre":
            tList.sort(key=self.book_get_genre, reverse=reverse)
        elif value == "price":
            tList.sort(key=self.book_get_price, reverse=reverse)
        elif value == "code-barres":
            tList.sort(key=self.book_get_code_barres, reverse=reverse)

    @staticmethod
    def book_get_author(elem):
        author = ""
        if elem is not None:
            author = elem.author.lower()
        return author

    @staticmethod
    def book_get_name(elem):
        title = ""
        if elem is not None:
            title = elem.title.lower()
        return title

    @staticmethod
    def book_get_genre(elem):
        genre = ""
        if elem is not None:
            genre = elem.genre.lower()
        return genre

    @staticmethod
    def book_get_price(elem):
        if elem is not None:
            return elem.price
        return 0

    @staticmethod
    def book_get_code_barres(elem):
        if elem is not None:
            return elem.code_barres
        return ""

    def original_sort_by(self, tDict, value: str, reverse=False):
        if value == "author":
            tDict.sort(key=self.original_book_get_author, reverse=reverse)
        elif value == "name":
            tDict.sort(key=self.original_book_get_name, reverse=reverse)
        elif value == "genre":
            tDict.sort(key=self.original_book_get_genre, reverse=reverse)
        elif value == "price":
            tDict.sort(key=self.original_book_get_price, reverse=reverse)
        elif value == "code-barres":
            tDict.sort(key=self.original_book_get_code_barres, reverse=reverse)

    @staticmethod
    def original_book_get_author(elem):
        return elem["Auteur"]

    @staticmethod
    def original_book_get_name(elem):
        return elem["Livre"]

    @staticmethod
    def original_book_get_genre(elem):
        return elem["Genre"]

    @staticmethod
    def original_book_get_price(elem):
        return elem["Prix"]

    @staticmethod
    def original_book_get_code_barres(elem):
        return elem["Code-barres"]
