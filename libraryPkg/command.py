import json


from libraryPkg.book import  Book
from libraryPkg.customer import Customer
from mainController import MainController


class Command():
    def __init__(self, number="",furnisher="",isbn="",livre="",auteur="",prix="", date=""):

        self.number = number
        self.furnisher = furnisher
        self.isbn = isbn
        self.book = livre
        self.author = auteur
        self.price = prix
        self.date = date

    def get_data(self):
        return {
            "num_command": self.number,
            "nom_fournisseur": self.furnisher,
            "ISBN": self.isbn,
            "titre_livre": self.book,
            "auteur": self.author,
            "prix": self.price,
            "date_commande": self.date
        }

    def charger_command(self, data):
        try:
            self.number = data["num_command"]
            self.furnisher = data["nom_fournisseur"]
            self.isbn = data["ISBN"]
            self.book = data["titre_livre"]
            self.author = data["auteur"]
            self.price = data["prix"]
            self.date = data["date_commande"]
        except:
            pass


class Commands():
    def __init__(self):
        self.commands = []

    def get_commands(self):
        return self.commands

    def charger(self):
        # lis le fichier json
        self.commands=[]
        try:
            with open("data_commands.json", 'r') as f:
                data = json.load(f)
                print("a chargé le fichier data_command")
            for clef in data.keys():
                command = Command()
                command.charger_command(data[clef])
                self.commands.append(command)
                print("Lit les clés commande")
                print("done")
            f.close()

        except:
            print("Le fichier data_command.json a échoué au chargement")

    def creercommand(self,numero,fournisseur,isbn,livre,auteur,prix,date_jour):
        command = Command(numero,fournisseur,isbn,livre,auteur,prix,date_jour)
        self.commands.append(command)
        print("La commande a été ajouté à la [] de commandes")

    def save_commands(self):
        data = {}
        i = 0
        for command in self.commands:
            data[i] = command.get_data()
            i += 1
        with open("data_commands.json", 'w') as f:
            json.dump(data,f,indent=7)
        f.close()

class Sale_Borrow():
    def __init__(self,id_client="",date="",storage="",shelve="",number=int,statusS=bool,statusB=bool):
        self.id_client = id_client
        self.storage = storage
        self.shelve = shelve
        self.book_number = number
        self.book = Book(storage, shelve, number)
        self.date = date
        self.book_borrowed = statusB
        self.book_sold = statusS
        self.customer = Customer(id_client)

    def get_data(self):
        return {

            "id_client": self.id_client,
            "armoire_livre": self.storage,
            "etagere_livre": self.shelve,
            "position_livre":self.book_number,
            "date_sortie": self.date,
            "Emprunte": self.book_borrowed,
            "Vendu": self.book_sold,
        }

    def charger_data(self, data):

        self.id_client = data["id_client"]
        self.storage = data["armoire_livre"]
        self.shelve = data["etagere_livre"]
        self.book_number = data["position_livre"]
        self.date = data["date_sortie"]
        self.book_borrowed = data["Emprunte"]
        self.book_sold = data["Vendu"]

class Sales_Borrows():
    def __init__(self):
        self.sales = []
        self.borrows = []

    def get_sales(self):
            return self.sales

    def get_borrows(self):
            return self.borrows

    def load_sales(self):
        # lis le fichier json des ventes
        try:
            with open("data_sales.json", 'r',encoding="utf-8") as f:
                data = json.load(f)
                print("a chargé le fichier ventes")
                for key in data.keys():
                    sale = Sale_Borrow()
                    sale.charger_data(data[key])
                    self.sales.append(sale)
                    print("Ventes Ajouté à borrows[]")
                f.close()
        except:
            print("Problème dans le chargement du fichier data_sales")



    def load_borrows(self):
        # lis le fichier json des emprunts
        try:
            with open("data_borrows.json", 'r',encoding="utf-8") as f:
                data = json.load(f)
                print("A chargé le fichier emprunts")
            for clef in data.keys():
                borrow = Sale_Borrow()
                borrow.charger_data(data[clef])
                self.borrows.append(borrow)
                print("Emprunts Ajouté à borrows[]")
            f.close()
        except:
            print("Ne charge pas le fichier data_borrows.json")

    def create_sale(self, id_client,date,storage,shelve,number,statusS,statusB):

        sale = Sale_Borrow(id_client,date,storage,shelve,number,statusS,statusB)
        if statusS == True and statusB == None:
            print("Le livre à l'emplacement "+sale.storage+str(sale.shelve)+str(sale.book_number)+" Est sold "+str(sale.book.sold))
            if sale.book.borrowed == False and sale.book.sold == False:
                self.sales.append(sale)
                print("Vente ajoutée")
                sale.book.sold = True

        else:
            print("Livre déjà vendu ou emprunté")

    def create_borrow(self,id_client,date,storage,shelve,number,statusS,statusB):

        borrow = Sale_Borrow(id_client,date,storage,shelve,number,statusS,statusB)
        print("Le livre à l'emplacement " + borrow.storage + str(borrow.shelve) + str(borrow.book_number) + " Est borrowed " + str(borrow.book.borrowed))
        if borrow.book.borrowed == False and borrow.book.sold == False:
            self.borrows.append(borrow)
            print("Emprunt ajouté")
            borrow.book.borrowed = True
            print("Le livre à l'emplacement " + borrow.storage + str(borrow.shelve) + str(
                borrow.book_number) + " Est borrowed " + str(borrow.book.borrowed))
        else:
            print("Livre déjà vendu ou emprunté")

    def save_sales(self):
        data = {}
        i = 0
        for sale in self.sales:
            data[i] = sale.get_data()
            i += 1
        with open("data_sales.json", 'w') as f:
            json.dump(data,f,indent=5)
        f.close()

    def save_borrows(self):
        data = {}
        i = 0
        for borrow in self.borrows:
            data[i] = borrow.get_data()
            i += 1
        with open("data_borrows.json", 'w') as f:
            json.dump(data,f,indent=5)
        f.close()