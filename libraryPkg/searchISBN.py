# -*- coding: iso-8859-1 -*
from urllib2 import urlopen

def recherche(ISBN):
    page = urlopen("http://france.isbn.pl/C-8/I-%s" % (ISBN))
    code = "".join(page.readlines())
    page.close()  # S�lectionne la ligne <title>...</title> qui contient les donn�es
    infos = (str([elem for elem in code.split("\n") if '<title>' in elem])[9:-10]).split(" - ")

    if infos[0][1] == "(":  # Cf. site: s'il ne trouve pas, le titre de la page
        return "Correspondance ISBN non trouv�e"  # commence par "(isbn ...)"

    else:
        auteur = infos[0]
        if "isbn" in infos[1]:  # Cf. site: V�rifie la pr�sence d'un sous-titre.
            titre0 = ''  # Exemple avec isbn 2915549133
            titre1 = infos[1][:-18]
        else:
            titre0 = str(infos[1]) + " - "
            titre1 = infos[2][:-18]

        return [auteur, titre0, titre1]


def correct(ISBN):
    if len(ISBN) != 10: return 0
    total = 0
    k = 0
    try:
        for i in ISBN:  # Voir codage d'un ISBN
            total += chiffre(ISBN[k], k) * (11 - range(1, 11)[k])  # infos dans la FAQ de http://www.isbn.org/
            k += 1
        if total % 11 == 0: return 1

    except:
        return 0


def chiffre(i, k):
    try:
        if int(i) in range(0, 10): return int(i)
    except:
        if (i == "X" or i == "x") and k == 9: return 10  # Seul le dernier chiffre peut valoir 10
        pass


def affichage(donnees):
    if "str" in str(type(donnees)): print
    donnees
    if "list" in str(type(donnees)): print
    "Auteur             : %s\nTitre de l'ouvrage : %s%s" % (donnees[0], donnees[1], donnees[2])


if __name__ == "__main__":

    ISBN = raw_input("ISBN : ")

    if correct(ISBN):
        affichage(recherche(ISBN))

    else:
        ans = raw_input("Code ISBN incorrect, recherche internet inutile. Essayer quand m�me ? (O)")
        if ans == "O" or ans == "o": affichage(recherche(ISBN))