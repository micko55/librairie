from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.scatterlayout import ScatterLayout


class Book(ScatterLayout):
    def __init__(self, storage, shelf, number, source="", author="", title="", genre="", price=0,
                 code_barres=0, **kwargs):
        super().__init__(**kwargs)
        self.index = 0
        self.image = Image(source=source, size=self.size, allow_stretch=True, keep_ratio=False)
        self.add_widget(self.image)
        self.allow_stretch = True
        self.keep_ratio = False
        self.number = number
        self.storage = storage
        self.shelf = shelf
        self.author = author
        self.title = title
        self.genre = genre
        self.price = price
        self.code_barres = code_barres
        self.borrowed = False
        self.return_time_left = 0
        self.sold = False
        self.label = None
        self.create_label()

    def on_touch_move(self, touch):
        pass

    def create_label(self):
        self.label = Label(text=self.title, color=(0, 0, 0, 1), valign="top", halign="center", shorten=True,
                           text_size=(self.width*.8, self.height), pos_hint={"x": -.05, "y": -.1}, font_size=13)
        self.label.original_font_size = 13
        self.add_widget(self.label)

    def get_data(self):
        return {
        "auteur": self.author,
        "position_etagere": self.number,
        "armoire": self.storage.name,
        "etagere": self.shelf.index,
        "titre": self.title,
        "genre": self.genre,
        "prix": self.price,
        "code_barre": self.code_barres,
        "emprunte": self.borrowed,
        "temps_avant_retour": self.return_time_left,
        "vendu": self.sold,
        }

    def charger_data(self, data):
        self.author = data["auteur"]
        self.number = data["position_etagere"]
        self.storage = data["armoire"]
        self.shelf.index = data["etagere"]
        self.title = data["titre"]
        self.genre = data["genre"]
        self.price = data["prix"]
        self.code_barres = data["code_barre"]
        self.borrowed = ["emprunte"]
        self.return_time_left = ["temps_avant_retou"]
        self.sold = data["vendu"]