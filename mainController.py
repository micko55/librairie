import json


class MainController:
    def __init__(self, parent, view):
        self._parent = parent
        self._view = view

    @property
    def parent(self):
        return self._parent

    @property
    def view(self):
        return self._view

    @parent.setter
    def parent(self, value):
        self._parent = value

    @view.setter
    def view(self, value):
        self._view = value

    def on_button_press(self, instance):
        pass

    @staticmethod
    def get_users():
        users = []
        try:
            with open("users.json", 'r', encoding="utf-8") as f:
                jsonDict = json.load(f)
                for key, value in jsonDict.items():
                    for i in range(len(value)):
                        users.append([value[i][0], value[i][1]])
        except UnicodeEncodeError:
            print("erf.")
        except FileNotFoundError:
            pass
        return users

    @staticmethod
    def open_popup(view, popup_id):
        view.add_widget(view.popups[popup_id], index=0)
        view.controller.popup_shown = popup_id

    @staticmethod
    def close_popup(instance):
        view = instance.parent.view
        view.controller.popup_shown = None
        instance.parent.popup.book = None
        view.remove_widget(instance.parent.popup)

    def change_view(self, button):
        self.parent.change_view(button.id)
        """if button.text == "Déconnexion":
            self.parent.change_view("login")
        elif button.text == "Mon Compte":
            self.parent.change_view("account")
        elif button.text == "Suivre Budget":
            self.parent.change_view("budget")
        elif button.text == "Retour Compte":
            self.parent.change_view("account")
        elif button.text == "Management":
            self.parent.change_view("management")
        elif button.text == "Mes Livres":
            self.parent.change_view("myBooks")
        elif button.text == "Envoyer":
            self.parent.change_view("sendBooks")
        elif button.text == "Ventes & Emprunts":
            self.parent.change_view("sales")
        elif button.text == "Nouvel Emprunt":
            self.parent.change_view("borrows")
        elif button.text == "Nouvelle Vente":
            self.parent.change_view("sales")
        elif button.text == "Historique Ventes":
            self.parent.change_view("historicSales")
        elif button.text == "Historique Emprunts":
            self.parent.change_view("historicBorrows")
        elif button.text == "?":
            self.parent.change_view("help")
        elif button.text == "Historique Commande":
            self.parent.change_view("historicCommands")
        elif button.text == "Retour menu":
            self.parent.change_view("sendBooks")
        elif button.text == "Retour menu Ventes":
            self.parent.change_view("sales")
        elif button.text == "Retour menu Emprunts":
            self.parent.change_view("borrows")"""


